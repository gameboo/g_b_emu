# g_b_emu #

g_b_emu is an emulation library !

## components ##

This directory contains the library's components

## interfaces ##

This directory contains the library's interfaces

## examples ##

This directory contains different test examples for the different interfaces and
components of the library

## and also ... ##

To make the lib :
- the interfaces_ target creates headers for the interfaces in a new
  *include/g_b_emu/interfaces* directory.
- the components_ target creates headers for the components in a new
  *include/g_b_emu/components* directory, and build object files for the
  different components. **interfaces_ is a dependecie of components_**
- the examples_ target builds the examples. **to build the examples, you need to
  have built all necessary interfaces and components before**

## nothing else so far :p ##

- yay !
- double yay !

gameboo

[That is me](http://www.ece.fr/~joannou) :)
come and say hi : <alexandre.joannou@gmail.com>
