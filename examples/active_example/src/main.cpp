#include "g_b_emu/interfaces/Active"

#include <iostream>
#include <string>

using namespace g_b::emu;
using namespace interfaces;
using namespace std;

#define ADDR_T unsigned int
#define DATA_T unsigned int

class TestActive : public Active
{
    private :
    string name;
    public :

    TestActive(string n, long double f) : Active(f)
    {
        name = n;
    }

    void exec()
    {
        static int a = 0;
        cout << name << a << endl;
        a++;
    }
};

int main()
{
    TestActive toto("toto", 0.5);
    TestActive tata("tata", 2);

    toto.createThread();
    tata.createThread();
    //sleep(4);
    //while(1);
    toto.joinThread();
    tata.joinThread();
}
