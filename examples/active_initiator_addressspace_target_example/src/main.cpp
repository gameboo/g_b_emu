#include "g_b_emu/interfaces/Initiator"
#include "g_b_emu/interfaces/Target"
#include "g_b_emu/components/AddressSpace"

#include <iostream>
#include <string>
#include <cassert>
#include <inttypes.h>

using namespace g_b::emu;
using namespace interfaces;
using namespace components;
using namespace std;

#define ADDR_T uint8_t
#define DATA_T uint8_t

template <typename addr_t, typename data_t>
class TestInitiator :
public Active,
public Initiator<addr_t,data_t>
{
    private :
    string name;

    enum target_enum{
    MAIN_TARGET = 0
    };

    public :

    TestInitiator(string n, const long double f) :
    Active(f),
    Initiator<data_t,addr_t>()
    {
        name = n;
    }

    void exec()
    {
        static int a = 0;
        cout << name << a << endl;

        assert(this->getNbTarget() > 0 &&
        "An Initiator must subscribe to an AdressSpace");

        cout <<
        this->target[MAIN_TARGET]->read((a*sizeof(data_t))%this->target[MAIN_TARGET]->getSize())
        << endl;

        a++;
    }
};

template <typename addr_t, typename data_t>
class TestTarget : public Target<addr_t,data_t>
{
    private :
    string name;

    public :

    TestTarget(string n, const unsigned long int size):
    Target<addr_t,data_t>(size)
    {
        name = n;
    }

    virtual data_t* getPointer(const addr_t& address)
    {
        return NULL;
    }

    virtual const data_t read(const addr_t& address) const
    {
        assert(address < this->size);
        if(address%2) return 42;
        else return 43;
    }

    virtual bool write(const addr_t& address,
                               const data_t& data)
    {
        assert(address < this->size);
        return true;
    }

    virtual bool write(const addr_t& address,
                               const data_t* data_ptr,
                               const unsigned int size)
    {
        assert(address < this->size);
        return false;
    }
};

int main()
{
    TestInitiator<ADDR_T,DATA_T> toto_init("toto_init", 0.5);
    TestInitiator<ADDR_T,DATA_T> tata_init("tata_init", 2);

    TestTarget<ADDR_T,DATA_T>    toto_target("toto_target", 0x0A);
    TestTarget<ADDR_T,DATA_T>    tata_target("tata_target", 0x14);

    // AddressSpace<ADDR_T,DATA_T,2>  addr_space;
    // not implemented in gcc version 4.4.5 (Debian 4.4.5-8)
    // gcc error is : "sorry, unimplemented: mangling fix_trunc_expr"
    AddressSpace<ADDR_T,DATA_T,2,200000>  addr_space;

    addr_space.map(toto_target, 0x0);
    addr_space.map(tata_target, 0xC);

    toto_init.setTarget(0,addr_space);
    tata_init.setTarget(0,addr_space);

    toto_init.createThread();
    tata_init.createThread();
    //sleep(4);
    //while(1);
    toto_init.joinThread();
    tata_init.joinThread();
}
