#include "g_b_emu/interfaces/Initiator"
#include "g_b_emu/components/RAM"
#include "g_b_emu/components/AddressSpace"

#include <iostream>
#include <string>
#include <cassert>
#include <inttypes.h>

using namespace g_b::emu;
using namespace interfaces;
using namespace components;
using namespace std;

//#define ADDR_T uint16_t
//#define DATA_T uint16_t
#define ADDR_T uint32_t
#define DATA_T uint32_t

template <typename data_t, typename addr_t>
class Writer :
public Active,
public Initiator<data_t,addr_t>
{
    private :
    string name;

    public :

    enum target_enum {
    MAIN_TARGET = 0
    };

    Writer(string n, const long double f) :
    Active(f),
    Initiator<addr_t,data_t>()
    {
        name = n;
    }

    void exec()
    {
        static int a = 0;
        cout << name << a << endl;

        assert(this->getNbTarget() > 0 &&
        "An Initiator must have at least one target");

        cout <<
        this->target[MAIN_TARGET]->write(0x0FAC, a)
        << endl;

        a++;
    }
};

template <typename data_t, typename addr_t>
class Reader :
public Active,
public Initiator<data_t,addr_t>
{
    private :
    string name;

    public :

    enum target_enum {
    MAIN_TARGET = 0
    };

    Reader(string n, const long double f) :
    Active(f),
    Initiator<addr_t,data_t>()
    {
        name = n;
    }

    void exec()
    {
        static int a = 0;
        cout << name << a << endl;

        assert(this->getNbTarget() > 0 &&
        "An Initiator must have at least one target");

        cout <<
        this->target[MAIN_TARGET]->read(0x0FAC)
        << endl;

        a++;
    }
};

int main()
{
    Writer<DATA_T,ADDR_T> writer("writer", 0.5);
    Reader<DATA_T,ADDR_T> reader("reader", 2);

    RAM<DATA_T,ADDR_T>    ram(0xFFFF + 1);

    AddressSpace<ADDR_T,DATA_T,1>  addr_space;
    // not implemented in gcc version 4.4.5 (Debian 4.4.5-8)
    // gcc error is : "sorry, unimplemented: mangling fix_trunc_expr"
    //AddressSpace<ADDR_T,DATA_T,1,200000>  addr_space;

    addr_space.map(ram, 0);

    writer.setTarget(Writer<DATA_T,ADDR_T>::MAIN_TARGET,addr_space);
    reader.setTarget(Reader<DATA_T,ADDR_T>::MAIN_TARGET,addr_space);

    writer.createThread();
    reader.createThread();
    //sleep(4);
    //while(1);
    writer.joinThread();
    reader.joinThread();
}
