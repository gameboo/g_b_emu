#include <g_b_emu/interfaces/Initiator>

#include <iostream>
#include <string>

using namespace g_b::emu;
using namespace interfaces;
using namespace std;

#define ADDR_T unsigned int
#define DATA_T unsigned int

class TestInitiator :
    public Active,
    public Initiator<DATA_T,ADDR_T>
{
    private :
    string name;
    public :

    TestInitiator(string n, const long double f) :
    Active(f),
    Initiator<DATA_T, ADDR_T>()
    {
        name = n;
    }

    void exec()
    {
        static int a = 0;
        cout << name << a << endl;
        a++;
    }
};

int main()
{
    TestInitiator toto("toto", 0.5);
    TestInitiator tata("tata", 2);

    toto.createThread();
    tata.createThread();
    //sleep(4);
    //while(1);
    toto.joinThread();
    tata.joinThread();
}
