#include "g_b_emu/components/R65XX"
#include "g_b_emu/components/ROM"
#include "g_b_emu/components/RAM"
#include "g_b_emu/components/AddressSpace"

#include <iostream>
#include <string>
#include <cstdlib>
#include <inttypes.h>

using namespace g_b::emu::components;
using namespace std;

#define ADDR_T 		uint32_t
#define DATA_T 		uint32_t
#define SOFT_NAME	"test.bin"

int main()
{
	R65XX 							cpu;
	RAM<DATA_T, ADDR_T>				ram(1 << 8);
	ROM<DATA_T, ADDR_T>				rom(1 << 8, SOFT_NAME);
    AddressSpace<DATA_T, ADDR_T, 2> addr_space;

//	addr_space.map();

	return EXIT_SUCCESS;
}
