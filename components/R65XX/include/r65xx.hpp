#ifndef G_B_EMU_R65XX_HPP
#define G_B_EMU_R65XX_HPP

/**
 * \file    r65xx.hpp
 * \brief   R65XX class
 * \author  Alexandre.JOANNOU
 * \version 0.1
 * \date    06/19/2012
 */

#include "g_b_emu/interfaces/Active"
#include "g_b_emu/interfaces/Initiator"
#include "g_b_emu/interfaces/Interruptible"

#include <inttypes.h>

namespace g_b
{
    namespace emu
    {
        namespace components
        {
            /**
             * \class   R65XX r65xx.hpp
             * \brief   Class representing a R65XX processor
             *
             * The R65XX is an Active component, an Initiator component, with
             * 8bit data and 16bit address, and a 3-times Interruptible
             * component. See the doc for more information.
             *
             */
            using interfaces::Active;
            using interfaces::Initiator;
            using interfaces::Interruptible;
            class R65XX :
                public Active,
                public Initiator<uint8_t,uint16_t>,
                public Interruptible<3>
            {
                /////////////
                // METHODS //
                /////////////

                public :
                    /**
                     * \brief   Constructor
                     *
                     * \param   frequency : the frequency of the thread
                     *
                     */
                    // freq value (1.79MHz) found here :
                    // http://nesdev.com/2A03%20technical%20reference.txt
                    R65XX(const long double freq = 1.79e6);
                    /**
                     * \brief   Destructor
                     */
                    virtual ~R65XX();

                    enum target_enum
                    {
                        MAIN = 0
                    };

                    enum interrupt_enum
                    {
                        RESET   =   0,
                        NMI     =   1,
                        IRQ     =   2
                    };

                    // Implement Active interface //
                    ////////////////////////////////////////////////////////////////////
                    /**
                     * \brief   See Active
                     */
                    virtual void exec();


                    ////////////////
                    // ATTRIBUTES //
                    ////////////////

                protected :

                    // CPU registers
                    uint8_t A;                      /*< The accumlator register*/
                    uint8_t X;                      /*< The X index register*/
                    uint8_t Y;                      /*< The Y index register*/
                    uint8_t S;                      /*< The stack pointer register*/
                    union reg16_t{
                        uint16_t value;             /*< The program counter register*/
                        struct{
                            uint8_t L : 8;          /*< The lower program counter register*/
                            uint8_t H : 8;          /*< The upper program counter register*/
                        };
                        reg16_t(int val = 0) :
                            value(val)
                        {}
                    } PC;                           /*< The program counter register wrapper*/
                    union regstatus_t{
                        uint8_t value;              /*< The status register*/
                        struct{
                            unsigned C : 1;         /*< The carry flag*/
                            unsigned Z : 1;         /*< The zero flag*/
                            unsigned I : 1;         /*< The IRQ disable flag*/
                            unsigned D : 1;         /*< The decimal mode flag*/
                            unsigned B : 1;         /*< The break command flag*/
                            unsigned r : 1;         /*< reserved  = 1*/
                            unsigned V : 1;         /*< The overflow flag*/
                            unsigned N : 1;         /*< The negative flag*/
                        };
                        regstatus_t(int val = 0x20) :
                            value(val)
                        {}
                    } STATUS;                       /*< The status register wrapper*/

                    #ifdef  CYCLE_ACCURATE
                    // CYCLE_ACCURATE variables
                    bool        boundary_crossed;
                    uint8_t     cycles_remaining;
                    uint8_t     oldPCH;
                    #endif

                    /////////////////////
                    // instruction set //
                    /////////////////////

                private :
                    //////////////////////
                    // addressing modes //
                    //////////////////////

                    static uint8_t * accumulator_mode           (R65XX *);
                    static uint8_t * immediate_mode             (R65XX *);
                    static uint8_t * zero_page_mode             (R65XX *);
                    static uint8_t * zero_page_X_mode           (R65XX *);
                    static uint8_t * zero_page_Y_mode           (R65XX *);
                    static uint8_t * absolute_mode              (R65XX *);
                    static uint8_t * absolute_X_mode            (R65XX *);
                    static uint8_t * absolute_Y_mode            (R65XX *);
                    static uint8_t * implied_mode               (R65XX *);
                    static uint8_t * relative_mode              (R65XX *);
                    static uint8_t * indexed_indirect_X_mode    (R65XX *);
                    static uint8_t * indirect_indexed_Y_mode    (R65XX *);
                    static uint8_t * absolute_indirect_mode     (R65XX *);

                    //////////////////
                    // instructions //
                    //////////////////
                    static void ADC     (R65XX *, uint8_t *);
                    static void AND     (R65XX *, uint8_t *);
                    static void ASL     (R65XX *, uint8_t *);
                    static void BCC     (R65XX *, uint8_t *);
                    static void BCS     (R65XX *, uint8_t *);
                    static void BEQ     (R65XX *, uint8_t *);
                    static void BIT     (R65XX *, uint8_t *);
                    static void BMI     (R65XX *, uint8_t *);
                    static void BNE     (R65XX *, uint8_t *);
                    static void BPL     (R65XX *, uint8_t *);
                    static void BRK     (R65XX *, uint8_t *);
                    static void BVC     (R65XX *, uint8_t *);
                    static void BVS     (R65XX *, uint8_t *);
                    static void CLC     (R65XX *, uint8_t *);
                    static void CLD     (R65XX *, uint8_t *);
                    static void CLI     (R65XX *, uint8_t *);
                    static void CLV     (R65XX *, uint8_t *);
                    static void CMP     (R65XX *, uint8_t *);
                    static void CPX     (R65XX *, uint8_t *);
                    static void CPY     (R65XX *, uint8_t *);
                    static void DEC     (R65XX *, uint8_t *);
                    static void DEX     (R65XX *, uint8_t *);
                    static void DEY     (R65XX *, uint8_t *);
                    static void EOR     (R65XX *, uint8_t *);
                    static void INC     (R65XX *, uint8_t *);
                    static void INX     (R65XX *, uint8_t *);
                    static void INY     (R65XX *, uint8_t *);
                    static void JMP     (R65XX *, uint8_t *);
                    static void JSR     (R65XX *, uint8_t *);
                    static void LDA     (R65XX *, uint8_t *);
                    static void LDX     (R65XX *, uint8_t *);
                    static void LDY     (R65XX *, uint8_t *);
                    static void LSR     (R65XX *, uint8_t *);
                    static void NOP     (R65XX *, uint8_t *);
                    static void ORA     (R65XX *, uint8_t *);
                    static void PHA     (R65XX *, uint8_t *);
                    static void PHP     (R65XX *, uint8_t *);
                    static void PLA     (R65XX *, uint8_t *);
                    static void PLP     (R65XX *, uint8_t *);
                    static void ROL     (R65XX *, uint8_t *);
                    static void ROR     (R65XX *, uint8_t *);
                    static void RTI     (R65XX *, uint8_t *);
                    static void RTS     (R65XX *, uint8_t *);
                    static void SBC     (R65XX *, uint8_t *);
                    static void SEC     (R65XX *, uint8_t *);
                    static void SED     (R65XX *, uint8_t *);
                    static void SEI     (R65XX *, uint8_t *);
                    static void STA     (R65XX *, uint8_t *);
                    static void STX     (R65XX *, uint8_t *);
                    static void STY     (R65XX *, uint8_t *);
                    static void TAX     (R65XX *, uint8_t *);
                    static void TAY     (R65XX *, uint8_t *);
                    static void TSX     (R65XX *, uint8_t *);
                    static void TXA     (R65XX *, uint8_t *);
                    static void TXS     (R65XX *, uint8_t *);
                    static void TYA     (R65XX *, uint8_t *);

                    ///////////////////
                    typedef uint8_t *   (*addressingModePtr) (R65XX *);
                    typedef void        (*instructionPtr)    (R65XX *, uint8_t *);
                    struct instructionWrapper_t
                    {
                        addressingModePtr   addressingMode;
                        instructionPtr      instruction;
                        uint8_t             nb_cycles;
                    };
                    static const instructionWrapper_t instructionTable[256];
            };
        }
    }
}

#endif
