#include "r65xx.hpp"

namespace g_b
{
    namespace emu
    {
        namespace components
        {
            uint8_t * R65XX::accumulator_mode(R65XX* r65xx)
			{
                //update PC
                r65xx->PC.value += 1; // 1-byte instructions
                return &r65xx->A; 
			}

            uint8_t * R65XX::immediate_mode(R65XX* r65xx)
			{
                reg16_t tmp;
                tmp.value = r65xx->PC.value;
                //update PC
                r65xx->PC.value += 2; // 2-byte instructions
                #ifdef  CYCLE_ACCURATE
                r65xx->boundary_crossed = (((reg16_t)(r65xx->PC.value-1)).H > r65xx->oldPCH);
                #endif
                return r65xx->target[MAIN]->getPointer(tmp.value);
			}

            uint8_t * R65XX::zero_page_mode(R65XX* r65xx)
			{
                reg16_t operand_addr;
                operand_addr.H = 0x00;
                operand_addr.L = r65xx->target[MAIN]->read(r65xx->PC.value+1);
                //update PC
                r65xx->PC.value += 2; // 2-byte instructions
                #ifdef  CYCLE_ACCURATE
                r65xx->boundary_crossed = (((reg16_t)(r65xx->PC.value-1)).H > r65xx->oldPCH);
                #endif
                return r65xx->target[MAIN]->getPointer(operand_addr.value);
			}

            uint8_t * R65XX::zero_page_X_mode(R65XX* r65xx)
			{
                reg16_t operand_addr;
                operand_addr.H = 0x00;
                operand_addr.L = r65xx->target[MAIN]->read(r65xx->PC.value+1)+r65xx->X;
                //update PC
                r65xx->PC.value += 2; // 2-byte instructions
                #ifdef  CYCLE_ACCURATE
                r65xx->boundary_crossed = (((reg16_t)(r65xx->PC.value-1)).H > r65xx->oldPCH);
                #endif
                return r65xx->target[MAIN]->getPointer(operand_addr.value);
			}

            uint8_t * R65XX::zero_page_Y_mode(R65XX* r65xx)
			{
                reg16_t operand_addr;
                operand_addr.H = 0x00;
                operand_addr.L = r65xx->target[MAIN]->read(r65xx->PC.value+1)+r65xx->Y;
                //update PC
                r65xx->PC.value += 2; // 2-byte instructions
                #ifdef  CYCLE_ACCURATE
                r65xx->boundary_crossed = (((reg16_t)(r65xx->PC.value-1)).H > r65xx->oldPCH);
                #endif
                return r65xx->target[MAIN]->getPointer(operand_addr.value);
			}

            uint8_t * R65XX::absolute_mode(R65XX* r65xx)
			{
                reg16_t operand_addr;
                operand_addr.H = r65xx->target[MAIN]->read(r65xx->PC.value+2);
                operand_addr.L = r65xx->target[MAIN]->read(r65xx->PC.value+1);
                //update PC
                r65xx->PC.value += 3; // 3-byte instructions
                #ifdef  CYCLE_ACCURATE
                r65xx->boundary_crossed = (((reg16_t)(r65xx->PC.value-1)).H > r65xx->oldPCH);
                #endif
                return r65xx->target[MAIN]->getPointer(operand_addr.value);
			}

            uint8_t * R65XX::absolute_X_mode(R65XX* r65xx)
			{
                reg16_t operand_addr;
                operand_addr.H = r65xx->target[MAIN]->read(r65xx->PC.value+2);
                operand_addr.L = r65xx->target[MAIN]->read(r65xx->PC.value+1);
                //update PC
                r65xx->PC.value += 3; // 3-byte instructions
                #ifdef  CYCLE_ACCURATE
                r65xx->boundary_crossed = (((reg16_t)(r65xx->PC.value-1)).H > r65xx->oldPCH);
                #endif
                return r65xx->target[MAIN]->getPointer(operand_addr.value + r65xx->X);
			}

            uint8_t * R65XX::absolute_Y_mode(R65XX* r65xx)
			{
                reg16_t operand_addr;
                operand_addr.H = r65xx->target[MAIN]->read(r65xx->PC.value+2);
                operand_addr.L = r65xx->target[MAIN]->read(r65xx->PC.value+1);
                //update PC
                r65xx->PC.value += 3; // 3-byte instructions
                #ifdef  CYCLE_ACCURATE
                r65xx->boundary_crossed = (((reg16_t)(r65xx->PC.value-1)).H > r65xx->oldPCH);
                #endif
                return r65xx->target[MAIN]->getPointer(operand_addr.value + r65xx->Y);
			}

            uint8_t * R65XX::implied_mode(R65XX* r65xx)
			{
                //update PC
                r65xx->PC.value += 1; // 1-byte instructions
                // no actual returned value
                return NULL;
			}

            uint8_t * R65XX::relative_mode(R65XX* r65xx)
			{
                //update PC
                r65xx->PC.value += 2; // 2-byte instructions
                // the returned value is a pointer to a branch offset (can be < 0)
                return r65xx->target[MAIN]->getPointer(r65xx->PC.value-1);
			}

            uint8_t * R65XX::indexed_indirect_X_mode(R65XX* r65xx)
			{
                reg16_t tmp1;
                reg16_t tmp2;
                reg16_t operand_addr;
                // init tmp1
                tmp1.H = 0x00;
                tmp1.L = r65xx->target[MAIN]->read(r65xx->PC.value+1)+r65xx->X+1;
                // init tmp2
                tmp2.H = 0x00;
                tmp2.L = r65xx->target[MAIN]->read(r65xx->PC.value+1)+r65xx->X;
                // init operand_addr
                operand_addr.H = r65xx->target[MAIN]->read(tmp1.value);
                operand_addr.L = r65xx->target[MAIN]->read(tmp2.value);
                //update PC
                r65xx->PC.value += 2; // 2-byte instructions
                #ifdef  CYCLE_ACCURATE
                r65xx->boundary_crossed = (((reg16_t)(r65xx->PC.value-1)).H > r65xx->oldPCH);
                #endif
                return r65xx->target[MAIN]->getPointer(operand_addr.value);
			}

            uint8_t * R65XX::indirect_indexed_Y_mode(R65XX* r65xx)
			{
                reg16_t tmp1;
                reg16_t tmp2;
                reg16_t operand_addr;
                // init tmp1
                tmp1.H = 0x00;
                tmp1.L = r65xx->target[MAIN]->read(r65xx->PC.value+1)+1;
                // init tmp2
                tmp2.H = 0x00;
                tmp2.L = r65xx->target[MAIN]->read(r65xx->PC.value+1);
                // init operand_addr
                operand_addr.H = r65xx->target[MAIN]->read(tmp1.value);
                operand_addr.L = r65xx->target[MAIN]->read(tmp2.value);
                //update PC
                r65xx->PC.value += 2; // 2-byte instructions
                #ifdef  CYCLE_ACCURATE
                r65xx->boundary_crossed = (((reg16_t)(r65xx->PC.value-1)).H > r65xx->oldPCH);
                #endif
                return r65xx->target[MAIN]->getPointer(operand_addr.value+r65xx->Y);
			}

            uint8_t * R65XX::absolute_indirect_mode(R65XX* r65xx)
			{
                // only updates the value of PC
                // /!\ only used with JMP
                reg16_t tmp;
                reg16_t tmp1;
                // init tmp
                tmp.H = r65xx->target[MAIN]->read(r65xx->PC.value+2);
                tmp.L = r65xx->target[MAIN]->read(r65xx->PC.value+1);
                // init tmp1
                tmp1.H = r65xx->target[MAIN]->read(tmp.value)+1;
                tmp1.L = r65xx->target[MAIN]->read(tmp.value);
                //update PC
                r65xx->PC.H = r65xx->target[MAIN]->read(tmp1.value+1);
                r65xx->PC.L = r65xx->target[MAIN]->read(tmp1.value);
                // no actual return value
                return NULL;
			}
        }
    }
}
