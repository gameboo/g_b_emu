#include "r65xx.hpp"

namespace g_b
{
    namespace emu
    {
        namespace components
        {
            //Performs a signed addition betwen the 2 8bits operands, returns
            //the result of the operation
            #define SGN_ADD(op1,op2)    (((op2) & 0x80) ? ((op1)-(128-((op2)&0x7F))) : ((op1)+(op2)))
            //Updates negative flag based on the given 8bit value, return the
            //new value of the flag
            #define UP_N(val)           (r65xx->STATUS.N = ((val) & 0x80) ? 1 : 0)
            //Updates zero flag based on the given 8bit value, returns the new
            //value of the flag
            #define UP_Z(val)           (r65xx->STATUS.Z = ((val) == 0  ) ? 1 : 0)
            //Performs first UP_N(val), then UP_Z(val), returns the new value of
            //the zero flag
            #define UP_NZ(val)          (UP_N(val), UP_Z(val))
            //The stack top address
            #define STACK_TOP           ((r65xx->S & 0x00FF) | 0x0100)
            //Pushes the given 8bit value on the top of the stack, decrements
            //the stack pointer and returns the new stack top address
            #define PUSH(val)           (r65xx->target[MAIN]->write(STACK_TOP,(val)), r65xx->S--)
            //Increments the stack pointer and pops the 8bit value from the top
            //of the stack, returning it
            #define POP()               (r65xx->S++, r65xx->target[MAIN]->read(STACK_TOP))

            #ifdef  CYCLE_ACCURATE
            //Increments the number of cycles if required by the
            //boundary_crossed indicator, returns the new value of cycles_remaining
            #define CHECK_BOUNDARY()    (r65xx->cycles_remaining += (r65xx->boundary_crossed) ? 1 : 0)
            //Increments the number of cycles depending on the jump destination,
            //returns the new value of cycles_remaining
            #define CHECK_PAGE()        (r65xx->cycles_remaining += (r65xx->oldPCH == r65xx->PC.H) ? 1 : 2)
            #endif

            void R65XX::ADC(R65XX * r65xx, uint8_t * pt_operand)
			{
                //perform the ADC
                int tmp;
                tmp = SGN_ADD(r65xx->A,*pt_operand) + r65xx->STATUS.C;
                //taking care of carry flag
                r65xx->STATUS.C = (tmp > 0xFF) ? 1 : 0;
                //taking care of overflow flag
                r65xx->STATUS.V =
                ((!((r65xx->A ^ *pt_operand) & 0x80)) &&
                ((r65xx->A ^ tmp) & 0x80)) ? 1 : 0;
                //affect accumulator register
                r65xx->A = tmp & 0xFF;
                //taking care of negative and zero flags
                UP_NZ(r65xx->A);

                #ifdef  CYCLE_ACCURATE
                //handles boundary crossing
                CHECK_BOUNDARY();
                #endif
			}

            void R65XX::AND(R65XX * r65xx, uint8_t * pt_operand)
			{
                //perform the AND
                r65xx->A &= *pt_operand;
                //taking care of negative and zero flags
                UP_NZ(r65xx->A);

                #ifdef  CYCLE_ACCURATE
                //handles boundary crossing
                CHECK_BOUNDARY();
                #endif
			}

            void R65XX::ASL(R65XX * r65xx, uint8_t * pt_operand)
			{
                //taking care of carry flag
                r65xx->STATUS.C = (*pt_operand & 0x80) ? 1 : 0;
                //shift one bit left
                *pt_operand <<= 1;
                //taking care of negative and zero flags
                UP_NZ(*pt_operand);
			}

            void R65XX::BCC(R65XX * r65xx, uint8_t * pt_operand)
			{
                //checks if branch occurs
                if(!r65xx->STATUS.C)
                {
                    //perform the branch
                    SGN_ADD(r65xx->PC.value,*pt_operand);

                    #ifdef  CYCLE_ACCURATE
                    //update cycles_remaining
                    CHECK_PAGE();
                    #endif
                }
			}

            void R65XX::BCS(R65XX * r65xx, uint8_t * pt_operand)
			{
                //checks if branch occurs
                if(r65xx->STATUS.C)
                {
                    //perform the branch
                    SGN_ADD(r65xx->PC.value,*pt_operand);

                    #ifdef  CYCLE_ACCURATE
                    //update cycles_remaining
                    CHECK_PAGE();
                    #endif
                }
			}

            void R65XX::BEQ(R65XX * r65xx, uint8_t * pt_operand)
			{
                //checks if branch occurs
                if(r65xx->STATUS.Z)
                {
                    //perform the branch
                    SGN_ADD(r65xx->PC.value,*pt_operand);

                    #ifdef  CYCLE_ACCURATE
                    //update cycles_remaining
                    CHECK_PAGE();
                    #endif
                }
			}

            void R65XX::BIT(R65XX * r65xx, uint8_t * pt_operand)
			{
                //update Z flag
                r65xx->STATUS.Z = ((r65xx->A & *pt_operand)==0) ? 1 : 0;
                //update N flag
                r65xx->STATUS.N = (*pt_operand & 0x80) ? 1 : 0;
                //update V flag
                r65xx->STATUS.V = (*pt_operand & 0x40) ? 1 : 0;
			}

            void R65XX::BMI(R65XX * r65xx, uint8_t * pt_operand)
			{
                //checks if branch occurs
                if(r65xx->STATUS.N)
                {
                    //perform the branch
                    SGN_ADD(r65xx->PC.value,*pt_operand);

                    #ifdef  CYCLE_ACCURATE
                    //update cycles_remaining
                    CHECK_PAGE();
                    #endif
                }
			}

            void R65XX::BNE(R65XX * r65xx, uint8_t * pt_operand)
			{
                //checks if branch occurs
                if(!r65xx->STATUS.Z)
                {
                    //perform the branch
                    SGN_ADD(r65xx->PC.value,*pt_operand);

                    #ifdef  CYCLE_ACCURATE
                    //update cycles_remaining
                    CHECK_PAGE();
                    #endif
                }
			}

            void R65XX::BPL(R65XX * r65xx, uint8_t * pt_operand)
			{
                //checks if branch occurs
                if(!r65xx->STATUS.N)
                {
                    //perform the branch
                    SGN_ADD(r65xx->PC.value,*pt_operand);

                    #ifdef  CYCLE_ACCURATE
                    //update cycles_remaining
                    CHECK_PAGE();
                    #endif
                }
			}

            void R65XX::BRK(R65XX * r65xx, uint8_t * pt_operand)
			{
                //triggers an irq
                r65xx->interruption[IRQ]    =   1;
                //set the break flag
                r65xx->STATUS.B             =   1;
			}

            void R65XX::BVC(R65XX * r65xx, uint8_t * pt_operand)
			{
                //checks if branch occurs
                if(!r65xx->STATUS.V)
                {
                    //perform the branch
                    SGN_ADD(r65xx->PC.value,*pt_operand);

                    #ifdef  CYCLE_ACCURATE
                    //update cycles_remaining
                    CHECK_PAGE();
                    #endif
                }
			}

            void R65XX::BVS(R65XX * r65xx, uint8_t * pt_operand)
			{
                //checks if branch occurs
                if(r65xx->STATUS.V)
                {
                    //perform the branch
                    SGN_ADD(r65xx->PC.value,*pt_operand);

                    #ifdef  CYCLE_ACCURATE
                    //update cycles_remaining
                    CHECK_PAGE();
                    #endif
                }
			}

            void R65XX::CLC(R65XX * r65xx, uint8_t * pt_operand)
			{
                //clears carry flag
                r65xx->STATUS.C = 0;
			}

            void R65XX::CLD(R65XX * r65xx, uint8_t * pt_operand)
			{
                //clears decimal mode flag
                r65xx->STATUS.D = 0;
			}

            void R65XX::CLI(R65XX * r65xx, uint8_t * pt_operand)
			{
                //clears irq disable flag
                r65xx->STATUS.I = 0;
			}

            void R65XX::CLV(R65XX * r65xx, uint8_t * pt_operand)
			{
                //clears overflow flag
                r65xx->STATUS.V = 0;
			}

            void R65XX::CMP(R65XX * r65xx, uint8_t * pt_operand)
			{
                //do the operation
                int tmp;
                tmp = r65xx->A - *pt_operand;
                //check for carry flag
                r65xx->STATUS.C = (tmp < 0x100) ? 1 : 0;
                //check for negative & zero flags
                UP_NZ(tmp & 0xFF);
			}

            void R65XX::CPX(R65XX * r65xx, uint8_t * pt_operand)
			{
                //do the operation
                int tmp;
                tmp = r65xx->X - *pt_operand;
                //check for carry flag
                r65xx->STATUS.C = (tmp < 0x100) ? 1 : 0;
                //check for negative and zero flags
                UP_NZ(tmp & 0xFF);
			}

            void R65XX::CPY(R65XX * r65xx, uint8_t * pt_operand)
			{
                //do the operation
                int tmp;
                tmp = r65xx->Y - *pt_operand;
                //check for carry flag
                r65xx->STATUS.C = (tmp < 0x100) ? 1 : 0;
                //check for negative & zero flags
                UP_NZ(tmp & 0xFF);
			}

            void R65XX::DEC(R65XX * r65xx, uint8_t * pt_operand)
			{
                //decrement operand
                *pt_operand--;
                //update negative and zero flags
                UP_NZ(*pt_operand);
			}

            void R65XX::DEX(R65XX * r65xx, uint8_t * pt_operand)
			{
                //decrement X index
                r65xx->X--;
                //update negative and zero flags
                UP_NZ(r65xx->X);
			}

            void R65XX::DEY(R65XX * r65xx, uint8_t * pt_operand)
			{
                //decrement Y index
                r65xx->Y--;
                //update negative and zero flags
                UP_NZ(r65xx->Y);
			}

            void R65XX::EOR(R65XX * r65xx, uint8_t * pt_operand)
			{
                //perform the EOR
                r65xx->A ^= *pt_operand;
                //taking care of negative and zero flags
                UP_NZ(r65xx->A);

                #ifdef  CYCLE_ACCURATE
                //handles boundary crossing
                CHECK_BOUNDARY();
                #endif
			}

            void R65XX::INC(R65XX * r65xx, uint8_t * pt_operand)
			{
                //increment operand
                *pt_operand++;
                //update negative and zero flags
                UP_NZ(*pt_operand);
			}

            void R65XX::INX(R65XX * r65xx, uint8_t * pt_operand)
			{
                //increment X index
                r65xx->X++;
                //update negative and zero flags
                UP_NZ(r65xx->X);
			}

            void R65XX::INY(R65XX * r65xx, uint8_t * pt_operand)
			{
                //increment Y index
                r65xx->Y++;
                //update negative and zero flags
                UP_NZ(r65xx->Y);
			}

            void R65XX::JMP(R65XX * r65xx, uint8_t * pt_operand)
			{
                if(pt_operand) //else already taken care of in indirect_mode
                {
                    //we're in absolute_mode
                    //get new PC
                    reg16_t operand_addr;
                    operand_addr.H = r65xx->target[MAIN]->read(r65xx->PC.value-1);
                    operand_addr.L = r65xx->target[MAIN]->read(r65xx->PC.value-2);
                    r65xx->PC.value = operand_addr.value;
                }
			}

            void R65XX::JSR(R65XX * r65xx, uint8_t * pt_operand)
			{
                //push return address (PC) on stack
                PUSH(r65xx->PC.H);
                PUSH(r65xx->PC.L);
                //we're in absolute_mode
                //get new PC
                reg16_t operand_addr;
                operand_addr.H = r65xx->target[MAIN]->read(r65xx->PC.value-1);
                operand_addr.L = r65xx->target[MAIN]->read(r65xx->PC.value-2);
                r65xx->PC.value = operand_addr.value;
			}

            void R65XX::LDA(R65XX * r65xx, uint8_t * pt_operand)
			{
                //perform the affectation
                r65xx->A = *pt_operand;
                //taking care of negative and zero flags
                UP_NZ(r65xx->A);

                #ifdef  CYCLE_ACCURATE
                //handles boundary crossing
                CHECK_BOUNDARY();
                #endif
			}

            void R65XX::LDX(R65XX * r65xx, uint8_t * pt_operand)
			{
                //perform the affectation
                r65xx->X = *pt_operand;
                //taking care of negative and zero flags
                UP_NZ(r65xx->X);

                #ifdef  CYCLE_ACCURATE
                //handles boundary crossing
                CHECK_BOUNDARY();
                #endif
			}

            void R65XX::LDY(R65XX * r65xx, uint8_t * pt_operand)
			{
                //perform the affectation
                r65xx->Y = *pt_operand;
                //taking care of negative and zero flags
                UP_NZ(r65xx->Y);

                #ifdef  CYCLE_ACCURATE
                //handles boundary crossing
                CHECK_BOUNDARY();
                #endif
			}

            void R65XX::LSR(R65XX * r65xx, uint8_t * pt_operand)
			{
                //taking care of carry flag
                r65xx->STATUS.C = (*pt_operand & 0x01) ? 1 : 0;
                //shift one bit right
                *pt_operand >>= 1;
                //taking care of negative and zero flags
                UP_NZ(*pt_operand);
			}

            void R65XX::NOP(R65XX * r65xx, uint8_t * pt_operand)
			{
                //does nothinq
			}

            void R65XX::ORA(R65XX * r65xx, uint8_t * pt_operand)
			{
                //perform the OR
                r65xx->A |= *pt_operand;
                //taking care of negative and zero flags
                UP_NZ(r65xx->A);

                #ifdef  CYCLE_ACCURATE
                //handles boundary crossing
                CHECK_BOUNDARY();
                #endif
			}

            void R65XX::PHA(R65XX * r65xx, uint8_t * pt_operand)
			{
                //push the accumulator on stack
                PUSH(r65xx->A);
			}

            void R65XX::PHP(R65XX * r65xx, uint8_t * pt_operand)
			{
                //push the status register on stack
                PUSH(r65xx->STATUS.value);
			}

            void R65XX::PLA(R65XX * r65xx, uint8_t * pt_operand)
			{
                //gets the value of the accumulator from the stack
                r65xx->A = POP();
                //taking care of negative and zero flags
                UP_NZ(r65xx->A);
			}

            void R65XX::PLP(R65XX * r65xx, uint8_t * pt_operand)
			{
                //gets the value of the status register from the stack
                r65xx->STATUS.value = POP();
			}

            void R65XX::ROL(R65XX * r65xx, uint8_t * pt_operand)
			{
                //save carry out
                bool carry_out = (*pt_operand & 0x80) ? true : false;
                //shift one bit left
                *pt_operand <<= 1;
                //insert last carry bit to the right
                *pt_operand |= (r65xx->STATUS.C) ? 0x01 : 0x00;
                //updates carry flag
                r65xx->STATUS.C = (carry_out) ? 1 : 0;
                //taking care of negative and zero flags
                UP_NZ(*pt_operand);
			}

            void R65XX::ROR(R65XX * r65xx, uint8_t * pt_operand)
			{
                //save carry out
                bool carry_out = (*pt_operand & 0x01) ? true : false;
                //shift one bit right
                *pt_operand >>= 1;
                //insert last carry bit to the right
                *pt_operand |= (r65xx->STATUS.C) ? 0x80 : 0x00;
                //updates carry flag
                r65xx->STATUS.C = (carry_out) ? 1 : 0;
                //taking care of negative and zero flags
                UP_NZ(*pt_operand);
			}

            void R65XX::RTI(R65XX * r65xx, uint8_t * pt_operand)
			{
                //get status register from the stack
                r65xx->STATUS.value = POP();
                //updates PC (get return address from the stack)
                r65xx->PC.L = POP();
                r65xx->PC.H = POP();
			}

            void R65XX::RTS(R65XX * r65xx, uint8_t * pt_operand)
			{
                //updates PC (get return address from the stack)
                r65xx->PC.L = POP();
                r65xx->PC.H = POP();
			}

            void R65XX::SBC(R65XX * r65xx, uint8_t * pt_operand)
			{
                // do the substraction
                int tmp = r65xx->A - *pt_operand;
                // handles borrow case
                tmp -= (!r65xx->STATUS.C) ? 1 : 0;
                //taking care of carry flag
                r65xx->STATUS.C = (tmp < 0x100) ? 1 : 0;
                //taking care of overflow flag
                r65xx->STATUS.V =
                ((!((r65xx->A ^ *pt_operand) & 0x80)) &&
                ((r65xx->A ^ tmp) & 0x80)) ? 1 : 0;
                //affect accumulator register
                r65xx->A = tmp & 0xFF;
                //taking care of negative and zero flags
                UP_NZ(r65xx->A);

                #ifdef  CYCLE_ACCURATE
                //handles boundary crossing
                CHECK_BOUNDARY();
                #endif
			}

            void R65XX::SEC(R65XX * r65xx, uint8_t * pt_operand)
			{
                //sets the carry flag
                r65xx->STATUS.C = 1;
			}

            void R65XX::SED(R65XX * r65xx, uint8_t * pt_operand)
			{
                //sets the decimal mode flag
                r65xx->STATUS.D = 1;
			}

            void R65XX::SEI(R65XX * r65xx, uint8_t * pt_operand)
			{
                //sets the irq disable flag
                r65xx->STATUS.I = 1;
			}

            void R65XX::STA(R65XX * r65xx, uint8_t * pt_operand)
			{
                //stores accumulator at pointed location
                *pt_operand = r65xx->A;
			}

            void R65XX::STX(R65XX * r65xx, uint8_t * pt_operand)
			{
                //stores X index at pointed location
                *pt_operand = r65xx->X;
			}

            void R65XX::STY(R65XX * r65xx, uint8_t * pt_operand)
			{
                //stores Y index at pointed location
                *pt_operand = r65xx->Y;
			}

            void R65XX::TAX(R65XX * r65xx, uint8_t * pt_operand)
			{
                //stores accumulator in X index
                r65xx->X = r65xx->A;
                //taking care of negative and zero flags
                UP_NZ(r65xx->A);
			}

            void R65XX::TAY(R65XX * r65xx, uint8_t * pt_operand)
			{
                //stores accumulator in Y index
                r65xx->Y = r65xx->A;
                //taking care of negative and zero flags
                UP_NZ(r65xx->A);
			}

            void R65XX::TSX(R65XX * r65xx, uint8_t * pt_operand)
			{
                //stores stack pointer in X index
                r65xx->X = r65xx->S;
                //taking care of negative and zero flags
                UP_NZ(r65xx->S);
			}

            void R65XX::TXA(R65XX * r65xx, uint8_t * pt_operand)
			{
                //stores X index in accumulator
                r65xx->A = r65xx->X;
                //taking care of negative and zero flags
                UP_NZ(r65xx->X);
			}

            void R65XX::TXS(R65XX * r65xx, uint8_t * pt_operand)
			{
                //stores X index in stack pointer
                r65xx->S = r65xx->X;
			}

            void R65XX::TYA(R65XX * r65xx, uint8_t * pt_operand)
			{
                //stores Y index in accumulator
                r65xx->A = r65xx->Y;
                //taking care of negative and zero flags
                UP_NZ(r65xx->Y);
			}

            #undef  SGN_ADD
            #undef  UP_N
            #undef  UP_Z
            #undef  UP_NZ
            #undef  STACK_TOP
            #undef  PUSH
            #undef  POP
            
            #ifdef  CYCLE_ACCURATE
            #undef  CHECK_BOUNDARY
            #undef  CHECK_PAGE
            #endif

        }
    }
}
