#include "r65xx.hpp"

namespace g_b
{
    namespace emu
    {
        namespace components
        {

            #define WRAP(addr_mode, inst, nb_cycles) {&(R65XX::addr_mode) , &(R65XX::inst), nb_cycles}

            const R65XX::instructionWrapper_t R65XX::instructionTable[256] =
            {
                WRAP(   implied_mode            ,   BRK    ,    7   ),  // 0x00
                WRAP(   indexed_indirect_X_mode ,   ORA    ,    6   ),  // 0x01
                    {   NULL                    ,   NULL   ,    0   },  // 0x02
                    {   NULL                    ,   NULL   ,    0   },  // 0x03
                    {   NULL                    ,   NULL   ,    0   },  // 0x04
                WRAP(   zero_page_mode          ,   ORA    ,    3   ),  // 0x05
                WRAP(   zero_page_mode          ,   ASL    ,    5   ),  // 0x06
                    {   NULL                    ,   NULL   ,    0   },  // 0x07
                WRAP(   implied_mode            ,   PHP    ,    3   ),  // 0x08
                WRAP(   immediate_mode          ,   ORA    ,    2   ),  // 0x09
                WRAP(   accumulator_mode        ,   ASL    ,    2   ),  // 0x0A
                    {   NULL                    ,   NULL   ,    0   },  // 0x0B
                    {   NULL                    ,   NULL   ,    0   },  // 0x0C
                WRAP(   absolute_mode           ,   ORA    ,    4   ),  // 0x0D
                WRAP(   absolute_mode           ,   ASL    ,    6   ),  // 0x0E
                    {   NULL                    ,   NULL   ,    0   },  // 0x0F

                WRAP(   relative_mode           ,   BPL    ,    2   ),  // 0x10
                WRAP(   indirect_indexed_Y_mode ,   ORA    ,    5   ),  // 0x11
                    {   NULL                    ,   NULL   ,    0   },  // 0x12
                    {   NULL                    ,   NULL   ,    0   },  // 0x13
                    {   NULL                    ,   NULL   ,    0   },  // 0x14
                WRAP(   zero_page_X_mode        ,   ORA    ,    4   ),  // 0x15
                WRAP(   zero_page_X_mode        ,   ASL    ,    6   ),  // 0x16
                    {   NULL                    ,   NULL   ,    0   },  // 0x17
                WRAP(   implied_mode            ,   CLC    ,    2   ),  // 0x18
                WRAP(   absolute_Y_mode         ,   ORA    ,    4   ),  // 0x19
                    {   NULL                    ,   NULL   ,    0   },  // 0x1A
                    {   NULL                    ,   NULL   ,    0   },  // 0x1B
                    {   NULL                    ,   NULL   ,    0   },  // 0x1C
                WRAP(   absolute_X_mode         ,   ORA    ,    4   ),  // 0x1D
                WRAP(   absolute_X_mode         ,   ASL    ,    7   ),  // 0x1E
                    {   NULL                    ,   NULL   ,    0   },  // 0x1F

                WRAP(   absolute_mode           ,   JSR    ,    6   ),  // 0x20
                WRAP(   indexed_indirect_X_mode ,   AND    ,    6   ),  // 0x21
                    {   NULL                    ,   NULL   ,    0   },  // 0x22
                    {   NULL                    ,   NULL   ,    0   },  // 0x23
                WRAP(   zero_page_mode          ,   BIT    ,    3   ),  // 0x24
                WRAP(   zero_page_mode          ,   AND    ,    3   ),  // 0x25
                WRAP(   zero_page_mode          ,   ROL    ,    5   ),  // 0x26
                    {   NULL                    ,   NULL   ,    0   },  // 0x27
                WRAP(   implied_mode            ,   PLP    ,    4   ),  // 0x28
                WRAP(   immediate_mode          ,   AND    ,    2   ),  // 0x29
                WRAP(   accumulator_mode        ,   ROL    ,    2   ),  // 0x2A
                    {   NULL                    ,   NULL   ,    0   },  // 0x2B
                WRAP(   absolute_mode           ,   BIT    ,    4   ),  // 0x2C
                WRAP(   absolute_mode           ,   AND    ,    4   ),  // 0x2D
                WRAP(   absolute_mode           ,   ROL    ,    6   ),  // 0x2E
                    {   NULL                    ,   NULL   ,    0   },  // 0x2F

                WRAP(   relative_mode           ,   BMI    ,    2   ),  // 0x30
                WRAP(   indirect_indexed_Y_mode ,   AND    ,    5   ),  // 0x31
                    {   NULL                    ,   NULL   ,    0   },  // 0x32
                    {   NULL                    ,   NULL   ,    0   },  // 0x33
                    {   NULL                    ,   NULL   ,    0   },  // 0x34
                WRAP(   zero_page_X_mode        ,   AND    ,    4   ),  // 0x35
                WRAP(   zero_page_X_mode        ,   ROL    ,    6   ),  // 0x36
                    {   NULL                    ,   NULL   ,    0   },  // 0x37
                WRAP(   implied_mode            ,   SEC    ,    2   ),  // 0x38
                WRAP(   absolute_Y_mode         ,   AND    ,    4   ),  // 0x39
                    {   NULL                    ,   NULL   ,    0   },  // 0x3A
                    {   NULL                    ,   NULL   ,    0   },  // 0x3B
                    {   NULL                    ,   NULL   ,    0   },  // 0x3C
                WRAP(   absolute_X_mode         ,   AND    ,    4   ),  // 0x3D
                WRAP(   absolute_X_mode         ,   ROL    ,    7   ),  // 0x3E
                    {   NULL                    ,   NULL   ,    0   },  // 0x3F

                WRAP(   implied_mode            ,   RTI    ,    6   ),  // 0x40
                WRAP(   indexed_indirect_X_mode ,   EOR    ,    6   ),  // 0x41
                    {   NULL                    ,   NULL   ,    0   },  // 0x42
                    {   NULL                    ,   NULL   ,    0   },  // 0x43
                    {   NULL                    ,   NULL   ,    0   },  // 0x44
                WRAP(   zero_page_mode          ,   EOR    ,    3   ),  // 0x45
                WRAP(   zero_page_mode          ,   LSR    ,    5   ),  // 0x46
                    {   NULL                    ,   NULL   ,    0   },  // 0x47
                WRAP(   implied_mode            ,   PHA    ,    3   ),  // 0x48
                WRAP(   immediate_mode          ,   EOR    ,    2   ),  // 0x49
                WRAP(   accumulator_mode        ,   LSR    ,    2   ),  // 0x4A
                    {   NULL                    ,   NULL   ,    0   },  // 0x4B
                WRAP(   absolute_mode           ,   JMP    ,    3   ),  // 0x4C
                WRAP(   absolute_mode           ,   EOR    ,    4   ),  // 0x4D
                WRAP(   absolute_mode           ,   LSR    ,    6   ),  // 0x4E
                    {   NULL                    ,   NULL   ,    0   },  // 0x4F

                WRAP(   relative_mode           ,   BVC    ,    2   ),  // 0x50
                WRAP(   indirect_indexed_Y_mode ,   EOR    ,    5   ),  // 0x51
                    {   NULL                    ,   NULL   ,    0   },  // 0x52
                    {   NULL                    ,   NULL   ,    0   },  // 0x53
                    {   NULL                    ,   NULL   ,    0   },  // 0x54
                WRAP(   zero_page_X_mode        ,   EOR    ,    4   ),  // 0x55
                WRAP(   zero_page_X_mode        ,   LSR    ,    6   ),  // 0x56
                    {   NULL                    ,   NULL   ,    0   },  // 0x57
                WRAP(   implied_mode            ,   CLI    ,    2   ),  // 0x58
                WRAP(   absolute_Y_mode         ,   EOR    ,    4   ),  // 0x59
                    {   NULL                    ,   NULL   ,    0   },  // 0x5A
                    {   NULL                    ,   NULL   ,    0   },  // 0x5B
                    {   NULL                    ,   NULL   ,    0   },  // 0x5C
                WRAP(   absolute_X_mode         ,   EOR    ,    4   ),  // 0x5D
                WRAP(   absolute_X_mode         ,   LSR    ,    7   ),  // 0x5E
                    {   NULL                    ,   NULL   ,    0   },  // 0x5F

                WRAP(   implied_mode            ,   RTS    ,    6   ),  // 0x60
                WRAP(   indexed_indirect_X_mode ,   ADC    ,    6   ),  // 0x61
                    {   NULL                    ,   NULL   ,    0   },  // 0x62
                    {   NULL                    ,   NULL   ,    0   },  // 0x63
                    {   NULL                    ,   NULL   ,    0   },  // 0x64
                WRAP(   zero_page_mode          ,   EOR    ,    3   ),  // 0x65
                WRAP(   zero_page_mode          ,   LSR    ,    5   ),  // 0x66
                    {   NULL                    ,   NULL   ,    0   },  // 0x67
                WRAP(   implied_mode            ,   PLA    ,    4   ),  // 0x68
                WRAP(   immediate_mode          ,   ADC    ,    2   ),  // 0x69
                WRAP(   accumulator_mode        ,   ROR    ,    2   ),  // 0x6A
                    {   NULL                    ,   NULL   ,    0   },  // 0x6B
                WRAP(   absolute_indirect_mode  ,   JMP    ,    5   ),  // 0x6C
                WRAP(   absolute_mode           ,   ADC    ,    4   ),  // 0x6D
                WRAP(   absolute_mode           ,   ROR    ,    6   ),  // 0x6E
                    {   NULL                    ,   NULL   ,    0   },  // 0x6F

                WRAP(   relative_mode           ,   BVS    ,    2   ),  // 0x70
                WRAP(   indirect_indexed_Y_mode ,   ADC    ,    5   ),  // 0x71
                    {   NULL                    ,   NULL   ,    0   },  // 0x72
                    {   NULL                    ,   NULL   ,    0   },  // 0x73
                    {   NULL                    ,   NULL   ,    0   },  // 0x74
                WRAP(   zero_page_X_mode        ,   ADC    ,    4   ),  // 0x75
                WRAP(   zero_page_X_mode        ,   ROR    ,    6   ),  // 0x76
                    {   NULL                    ,   NULL   ,    0   },  // 0x77
                WRAP(   implied_mode            ,   SEI    ,    2   ),  // 0x78
                WRAP(   absolute_Y_mode         ,   ADC    ,    4   ),  // 0x79
                    {   NULL                    ,   NULL   ,    0   },  // 0x7A
                    {   NULL                    ,   NULL   ,    0   },  // 0x7B
                    {   NULL                    ,   NULL   ,    0   },  // 0x7C
                WRAP(   absolute_X_mode         ,   ADC    ,    4   ),  // 0x7D
                WRAP(   absolute_X_mode         ,   ROR    ,    7   ),  // 0x7E
                    {   NULL                    ,   NULL   ,    0   },  // 0x7F

                    {   NULL                    ,   NULL   ,    0   },  // 0x80
                WRAP(   indexed_indirect_X_mode ,   STA    ,    6   ),  // 0x81
                    {   NULL                    ,   NULL   ,    0   },  // 0x82
                    {   NULL                    ,   NULL   ,    0   },  // 0x83
                WRAP(   zero_page_mode          ,   STY    ,    3   ),  // 0x84
                WRAP(   zero_page_mode          ,   STA    ,    3   ),  // 0x85
                WRAP(   zero_page_mode          ,   STX    ,    3   ),  // 0x86
                    {   NULL                    ,   NULL   ,    0   },  // 0x87
                WRAP(   implied_mode            ,   DEY    ,    2   ),  // 0x88
                    {   NULL                    ,   NULL   ,    0   },  // 0x89
                WRAP(   implied_mode            ,   TXA    ,    2   ),  // 0x8A
                    {   NULL                    ,   NULL   ,    0   },  // 0x8B
                WRAP(   absolute_mode           ,   STY    ,    4   ),  // 0x8C
                WRAP(   absolute_mode           ,   STA    ,    4   ),  // 0x8D
                WRAP(   absolute_mode           ,   STX    ,    4   ),  // 0x8E
                    {   NULL                    ,   NULL   ,    0   },  // 0x8F

                WRAP(   relative_mode           ,   BCC    ,    2   ),  // 0x90
                WRAP(   indirect_indexed_Y_mode ,   STA    ,    6   ),  // 0x91
                    {   NULL                    ,   NULL   ,    0   },  // 0x92
                    {   NULL                    ,   NULL   ,    0   },  // 0x93
                WRAP(   zero_page_X_mode        ,   STY    ,    4   ),  // 0x94
                WRAP(   zero_page_X_mode        ,   STA    ,    4   ),  // 0x95
                WRAP(   zero_page_Y_mode        ,   STX    ,    4   ),  // 0x96
                    {   NULL                    ,   NULL   ,    0   },  // 0x97
                WRAP(   implied_mode            ,   TYA    ,    2   ),  // 0x98
                WRAP(   absolute_Y_mode         ,   STA    ,    5   ),  // 0x99
                WRAP(   implied_mode            ,   TXS    ,    2   ),  // 0x9A
                    {   NULL                    ,   NULL   ,    0   },  // 0x9B
                    {   NULL                    ,   NULL   ,    0   },  // 0x9C
                WRAP(   absolute_X_mode         ,   STA    ,    5   ),  // 0x9D
                    {   NULL                    ,   NULL   ,    0   },  // 0x9E
                    {   NULL                    ,   NULL   ,    0   },  // 0x9F

                WRAP(   immediate_mode          ,   LDY    ,    2   ),  // 0xA0
                WRAP(   indexed_indirect_X_mode ,   LDA    ,    6   ),  // 0xA1
                WRAP(   immediate_mode          ,   LDX    ,    2   ),  // 0xA2
                    {   NULL                    ,   NULL   ,    0   },  // 0xA3
                WRAP(   zero_page_mode          ,   LDY    ,    3   ),  // 0xA4
                WRAP(   zero_page_mode          ,   LDA    ,    3   ),  // 0xA5
                WRAP(   zero_page_mode          ,   LDX    ,    3   ),  // 0xA6
                    {   NULL                    ,   NULL   ,    0   },  // 0xA7
                WRAP(   implied_mode            ,   TAY    ,    2   ),  // 0xA8
                WRAP(   immediate_mode          ,   LDA    ,    2   ),  // 0xA9
                WRAP(   implied_mode            ,   TAX    ,    2   ),  // 0xAA
                    {   NULL                    ,   NULL   ,    0   },  // 0xAB
                WRAP(   absolute_mode           ,   LDY    ,    4   ),  // 0xAC
                WRAP(   absolute_mode           ,   LDA    ,    4   ),  // 0xAD
                WRAP(   absolute_mode           ,   LDX    ,    4   ),  // 0xAE
                    {   NULL                    ,   NULL   ,    0   },  // 0xAF

                WRAP(   relative_mode           ,   BCS    ,    2   ),  // 0xB0
                WRAP(   indirect_indexed_Y_mode ,   LDA    ,    5   ),  // 0xB1
                    {   NULL                    ,   NULL   ,    0   },  // 0xB2
                    {   NULL                    ,   NULL   ,    0   },  // 0xB3
                WRAP(   zero_page_X_mode        ,   LDY    ,    4   ),  // 0xB4
                WRAP(   zero_page_X_mode        ,   LDA    ,    4   ),  // 0xB5
                WRAP(   zero_page_Y_mode        ,   LDX    ,    4   ),  // 0xB6
                    {   NULL                    ,   NULL   ,    0   },  // 0xB7
                WRAP(   implied_mode            ,   CLV    ,    2   ),  // 0xB8
                WRAP(   absolute_Y_mode         ,   LDA    ,    4   ),  // 0xB9
                WRAP(   implied_mode            ,   TSX    ,    2   ),  // 0xBA
                    {   NULL                    ,   NULL   ,    0   },  // 0xBB
                WRAP(   absolute_X_mode         ,   LDY    ,    4   ),  // 0xBC
                WRAP(   absolute_X_mode         ,   LDA    ,    4   ),  // 0xBD
                WRAP(   absolute_Y_mode         ,   LDX    ,    4   ),  // 0xBE
                    {   NULL                    ,   NULL   ,    0   },  // 0xBF

                WRAP(   immediate_mode          ,   CPY    ,    2   ),  // 0xC0
                WRAP(   indexed_indirect_X_mode ,   CMP    ,    6   ),  // 0xC1
                    {   NULL                    ,   NULL   ,    0   },  // 0xC2
                    {   NULL                    ,   NULL   ,    0   },  // 0xC3
                WRAP(   zero_page_mode          ,   CPY    ,    3   ),  // 0xC4
                WRAP(   zero_page_mode          ,   CMP    ,    3   ),  // 0xC5
                WRAP(   zero_page_mode          ,   DEC    ,    5   ),  // 0xC6
                    {   NULL                    ,   NULL   ,    0   },  // 0xC7
                WRAP(   implied_mode            ,   INY    ,    2   ),  // 0xC8
                WRAP(   immediate_mode          ,   CMP    ,    2   ),  // 0xC9
                WRAP(   implied_mode            ,   DEX    ,    2   ),  // 0xCA
                    {   NULL                    ,   NULL   ,    0   },  // 0xCB
                WRAP(   absolute_mode           ,   CPY    ,    4   ),  // 0xCC
                WRAP(   absolute_mode           ,   CMP    ,    4   ),  // 0xCD
                WRAP(   absolute_mode           ,   DEC    ,    6   ),  // 0xCE
                    {   NULL                    ,   NULL   ,    0   },  // 0xCF

                WRAP(   relative_mode           ,   BNE    ,    2   ),  // 0xD0
                WRAP(   indirect_indexed_Y_mode ,   CMP    ,    5   ),  // 0xD1
                    {   NULL                    ,   NULL   ,    0   },  // 0xD2
                    {   NULL                    ,   NULL   ,    0   },  // 0xD3
                    {   NULL                    ,   NULL   ,    0   },  // 0xD4
                WRAP(   zero_page_X_mode        ,   CMP    ,    4   ),  // 0xD5
                WRAP(   zero_page_X_mode        ,   DEC    ,    6   ),  // 0xD6
                    {   NULL                    ,   NULL   ,    0   },  // 0xD7
                WRAP(   implied_mode            ,   CLD    ,    2   ),  // 0xD8
                WRAP(   absolute_Y_mode         ,   CMP    ,    4   ),  // 0xD9
                    {   NULL                    ,   NULL   ,    0   },  // 0xDA
                    {   NULL                    ,   NULL   ,    0   },  // 0xDB
                    {   NULL                    ,   NULL   ,    0   },  // 0xDC
                WRAP(   absolute_X_mode         ,   CMP    ,    4   ),  // 0xDD
                WRAP(   absolute_X_mode         ,   DEC    ,    7   ),  // 0xDE
                    {   NULL                    ,   NULL   ,    0   },  // 0xDF

                WRAP(   immediate_mode          ,   CPX    ,    2   ),  // 0xE0
                WRAP(   indexed_indirect_X_mode ,   SBC    ,    6   ),  // 0xE1
                    {   NULL                    ,   NULL   ,    0   },  // 0xE2
                    {   NULL                    ,   NULL   ,    0   },  // 0xE3
                WRAP(   zero_page_mode          ,   CPX    ,    3   ),  // 0xE4
                WRAP(   zero_page_mode          ,   SBC    ,    3   ),  // 0xE5
                WRAP(   zero_page_mode          ,   INC    ,    5   ),  // 0xE6
                    {   NULL                    ,   NULL   ,    0   },  // 0xE7
                WRAP(   implied_mode            ,   INX    ,    2   ),  // 0xE8
                WRAP(   immediate_mode          ,   SBC    ,    2   ),  // 0xE9
                WRAP(   implied_mode            ,   NOP    ,    2   ),  // 0xEA
                    {   NULL                    ,   NULL   ,    0   },  // 0xEB
                WRAP(   absolute_mode           ,   CPX    ,    4   ),  // 0xEC
                WRAP(   absolute_mode           ,   SBC    ,    4   ),  // 0xED
                WRAP(   absolute_mode           ,   INC    ,    6   ),  // 0xEE
                    {   NULL                    ,   NULL   ,    0   },  // 0xEF

                WRAP(   relative_mode           ,   BEQ    ,    2   ),  // 0xF0
                WRAP(   indirect_indexed_Y_mode ,   SBC    ,    5   ),  // 0xF1
                    {   NULL                    ,   NULL   ,    0   },  // 0xF2
                    {   NULL                    ,   NULL   ,    0   },  // 0xF3
                    {   NULL                    ,   NULL   ,    0   },  // 0xF4
                WRAP(   zero_page_X_mode        ,   SBC    ,    4   ),  // 0xF5
                WRAP(   zero_page_X_mode        ,   INC    ,    6   ),  // 0xF6
                    {   NULL                    ,   NULL   ,    0   },  // 0xF7
                WRAP(   implied_mode            ,   SED    ,    2   ),  // 0xF8
                WRAP(   absolute_Y_mode         ,   SBC    ,    4   ),  // 0xF9
                    {   NULL                    ,   NULL   ,    0   },  // 0xFA
                    {   NULL                    ,   NULL   ,    0   },  // 0xFB
                    {   NULL                    ,   NULL   ,    0   },  // 0xFC
                WRAP(   absolute_X_mode         ,   SBC    ,    4   ),  // 0xFD
                WRAP(   absolute_X_mode         ,   INC    ,    7   ),  // 0xFE
                    {   NULL                    ,   NULL   ,    0   },  // 0xFF
            };

            #undef WRAP

        }
    }
}
