#include "r65xx.hpp"

namespace g_b
{
    namespace emu
    {
        namespace components
        {
            ////////////////////////////////////////////////////////////////////
            R65XX::R65XX(const long double freq):
                Active(freq),
                Initiator<uint8_t,uint16_t>(),
                Interruptible<3>(),
                A(0),
                X(0),
                Y(0),
                S(0),
                PC(0),//TODO reset policy ?
                STATUS(0)
                #ifdef  CYCLE_ACCURATE
                ,
                boundary_crossed(false),
                cycles_remaining(0),
                oldPCH(0)
                #endif
            {
            }
            ////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////
            R65XX::~R65XX()
            {
            }
            ////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////
            void R65XX::exec()
            {
            //check for number of cycles remaining (if feature enabled)
            #ifdef  CYCLE_ACCURATE
            cycles_remaining--;
            if(cycles_remaining <= 0)
            {
            //reset CYCLE_ACCURATE variables
            boundary_crossed    =   false   ;
            cycles_remaining    =   0       ;
            oldPCH              =   PC.H    ;
            #endif

                //check for interrupts
                #define SAVE_CTXT() ((target[MAIN]->write((S & 0x00FF) | 0x0100, PC.H), S--),\
                                     (target[MAIN]->write((S & 0x00FF) | 0x0100, PC.L), S--),\
                                     (target[MAIN]->write((S & 0x00FF) | 0x0100, STATUS.value), S--))

                if(interruption[RESET])
                {
                    SAVE_CTXT();
                    //jump to interrupt vector
                    PC.L                =   target[MAIN]->read(0XFFFC);
                    PC.H                =   target[MAIN]->read(0XFFFD);
                    #ifdef  CYCLE_ACCURATE
                    cycles_remaining    =   6;
                    #endif
                }
                else if(interruption[NMI])
                {
                    SAVE_CTXT();
                    //jump to interrupt vector
                    PC.L                =   target[MAIN]->read(0XFFFA);
                    PC.H                =   target[MAIN]->read(0XFFFB);
                    #ifdef  CYCLE_ACCURATE
                    cycles_remaining    =   6;
                    #endif
                }
                else if(interruption[IRQ] && !STATUS.I)
                {
                    SAVE_CTXT();
                    //mask other irq
                    STATUS.I            =   1;
                    //jump to interrupt vector
                    PC.L                =   target[MAIN]->read(0XFFFE);
                    PC.H                =   target[MAIN]->read(0XFFFF);
                    #ifdef  CYCLE_ACCURATE
                    cycles_remaining    =   6;
                    #endif
                }

                #undef SAVE_CTXT

                //fetch and do the next instruction
                instructionWrapper_t insWrap;
                insWrap = instructionTable[target[MAIN]->read(PC.value)];
                assert(insWrap.addressingMode && insWrap.instruction
                        && "unsupported opcode" );
                #ifdef  CYCLE_ACCURATE
                cycles_remaining        +=  insWrap.nb_cycles;
                #endif
                insWrap.instruction(this,insWrap.addressingMode(this));

                //check for number of cycles remaining (if feature enabled)
            #ifdef  CYCLE_ACCURATE
            }
            #endif
            }
            ////////////////////////////////////////////////////////////////////
        }
    }
}
