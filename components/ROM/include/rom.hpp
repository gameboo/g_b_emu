#ifndef G_B_EMU_COMPONENTS_ROM_HPP
#define G_B_EMU_COMPONENTS_ROM_HPP

#include "g_b_emu/interfaces/Target"
#include <fstream>
#include <cassert>

namespace g_b
{
    namespace emu
    {
        namespace components
        {
            using interfaces::Target;
            template<typename data_t, typename addr_t>
            class ROM : public Target<data_t, addr_t>
            {
                /////////////
                // METHODS //
                /////////////

                public :

                ////////////////////////////////////////////////////////////////////
                /**
                * \brief   Constructor
                */
                ROM(const unsigned long int size, const char * filename); // size in bytes
                ////////////////////////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////////
                /**
                * \brief   Destructor
                */
                virtual ~ROM();
                ////////////////////////////////////////////////////////////////////

                // Implement Target interface //
                ////////////////////////////////////////////////////////////////////
                /**
                * \brief   See Target
                */
                virtual data_t* getPointer(const addr_t& address);

                /**
                * \brief   See Target
                */
                virtual const data_t read(const addr_t& address) const;

                /**
                * \brief   See Target
                */
                virtual bool write(const addr_t& address,
                                   const data_t& data);

                /**
                * \brief   See Target
                */
                virtual bool write(const addr_t& address,
                                           const data_t* data_ptr,
                                           const unsigned int size);
                ////////////////////////////////////////////////////////////////////

                ////////////////
                // ATTRIBUTES //
                ////////////////

                protected :

				mutable std::ifstream rom_file;
            };
        }
    }
}

#endif
