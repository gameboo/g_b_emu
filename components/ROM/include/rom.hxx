#ifndef G_B_EMU_COMPONENTS_ROM_HXX
#define G_B_EMU_COMPONENTS_ROM_HXX

#include "rom.hpp"

namespace g_b
{
    namespace emu
    {
        namespace components
        {
            #define tmpl(...)\
            template<typename data_t, typename addr_t> \
            __VA_ARGS__ ROM<data_t, addr_t>

            ////////////////////////////////////////////////////////////////////
            tmpl()::ROM(const unsigned long int size, const char * filename) :
            Target<data_t,addr_t>(size),
			rom_file(filename, std::ios::binary)
            {
				assert(rom_file.is_open() && "Error finding or opening file");
            }
            ////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////
            tmpl()::~ROM()
            {
				if (rom_file.is_open())
					rom_file.close();
            }
            ////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////
            tmpl(data_t*)::getPointer(const addr_t& address)
            {
                assert(false && "getPointer should not be called on a ROM object");
				return NULL;
            }
            ////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////
            tmpl(const data_t)::read(const addr_t& address) const
            {
				char tmp[sizeof(data_t)];

				rom_file.seekg(address);
				rom_file.read(tmp, sizeof(data_t));
            	
				return (data_t) *tmp;
			}
            ////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////
            tmpl(bool)::write(const addr_t& address, const data_t& data)
            {
                assert(false && "write should not be called on a ROM object");
				return false;
            }
            ////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////
            tmpl(bool)::write(const addr_t& address,
                              const data_t* data_ptr,
                              const unsigned int size)
            {
                assert(false && "write should not be called on a ROM object");
				return false;
            }
            ////////////////////////////////////////////////////////////////////

            #undef tmpl
        }
    }
}

#endif
