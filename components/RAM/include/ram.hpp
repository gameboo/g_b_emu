#ifndef G_B_EMU_COMPONENTS_RAM_HPP
#define G_B_EMU_COMPONENTS_RAM_HPP

/**
 * \file    ram.hpp
 * \brief   RAM class
 * \author  Alexandre.JOANNOU
 * \version 0.1
 * \date    06/19/2012
 */

#include "g_b_emu/interfaces/Target"
#include <cassert>

namespace g_b
{
    namespace emu
    {
        namespace components
        {
            /**
             * \class   RAM ram.hpp
             * \brief   Class representing ram component
             *
             * A Ram is a Target. It stores the data read and written by the
             * initiators. As a Target, it has to be mapped in an AddressSpace
             * in order to be used by an Iniator.
             *
             */
            using interfaces::Target;
            template<typename data_t, typename addr_t>
            class RAM : public Target<data_t, addr_t>
            {
                /////////////
                // METHODS //
                /////////////

                public :

                ////////////////////////////////////////////////////////////////////
                /**
                * \brief   Constructor
                */
                RAM(const unsigned long int size);  // size in bytes
                ////////////////////////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////////
                /**
                * \brief   Destructor
                */
                virtual ~RAM();
                ////////////////////////////////////////////////////////////////////

                // Implement Target interface //
                ////////////////////////////////////////////////////////////////////
                /**
                * \brief   See Target
                */
                virtual data_t* getPointer(const addr_t& address);

                /**
                * \brief   See Target
                */
                virtual const data_t read(const addr_t& address) const;

                /**
                * \brief   See Target
                */
                virtual bool write(const addr_t& address,
                                   const data_t& data);

                /**
                * \brief   See Target
                */
                virtual bool write(const addr_t& address,
                                           const data_t* data_ptr,
                                           const unsigned int size);
                ////////////////////////////////////////////////////////////////////

                ////////////////
                // ATTRIBUTES //
                ////////////////

                protected :

                data_t * stored_data;    /*< The array of stored data*/
            };
        }
    }
}

#endif
