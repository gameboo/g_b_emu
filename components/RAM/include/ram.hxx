#ifndef G_B_EMU_COMPONENTS_RAM_HXX
#define G_B_EMU_COMPONENTS_RAM_HXX

#include "ram.hpp"

namespace g_b
{
    namespace emu
    {
        namespace components
        {
            #define tmpl(...)\
            template<typename data_t, typename addr_t> \
            __VA_ARGS__ RAM<data_t, addr_t>

            ////////////////////////////////////////////////////////////////////
            tmpl()::RAM(const unsigned long int s) :
            Target<data_t,addr_t>(s)
            {
                stored_data = new data_t[s];
            }
            ////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////
            tmpl()::~RAM()
            {
                delete [] stored_data;
            }
            ////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////
            tmpl(data_t*)::getPointer(const addr_t& address)
            {
                assert(address < this->size);
                return &(stored_data[address]);
            }
            ////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////
            tmpl(const data_t)::read(const addr_t& address) const
            {
                assert(address < this->size);
                return stored_data[address];
            }
            ////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////
            tmpl(bool)::write(const addr_t& address, const data_t& data)
            {
                assert(address < this->size);
                stored_data[address] = data;
                return true;
            }
            ////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////
            tmpl(bool)::write(const addr_t& address,
                              const data_t* data_ptr,
                              const unsigned int size)
            {
                assert((address + size - 1) < this->size);
                for(int i = 0; i < size; i++)
                {
                    stored_data[address+i] = data_ptr[i];
                }
                return true;
            }
            ////////////////////////////////////////////////////////////////////

            #undef tmpl
        }
    }
}

#endif
