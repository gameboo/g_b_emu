#ifndef G_B_EMU_COMPONENTS_ADDRESSSPACE_HPP
#define G_B_EMU_COMPONENTS_ADDRESSSPACE_HPP

/**
 * \file    addressspace.hpp
 * \brief   AdressSpace class
 * \author  Alexandre.JOANNOU
 * \version 0.1
 * \date    06/19/2012
 */

#include "g_b_emu/interfaces/Target"
#include "g_b_emu/interfaces/Initiator"
#include <cassert>
#include <cmath>

using namespace std;

namespace g_b
{
    namespace emu
    {
        namespace components
        {
            using interfaces::Target;
            using interfaces::Initiator;
            /**
             * \class   AddressSpace components/AddressSpace
             *
             * \brief   Class representing an address space
             *
             * \tparam  data_t   the data type used by the AddressSpace
             * \tparam  addr_t   the address type used by the AddressSpace
             * \tparam  nb_tgt   the number of desired Targets
             * \tparam  size     the desired size (in bytes) for the
             * AddressSpace (default value is max possible value given the used
             * address type) (has to be a multiple of data_t)
             *
             * An AddressSpace is a Target and an Initiator on several (or just
             * one) Targets. It allows for other Initiators to concentrate
             * their requests on just one Target (the AddressSpace), on wich
             * some other Targets are mapped.
             *
             */
            template<typename data_t,
                     typename addr_t,
                     const unsigned long int nb_tgt,
                     const unsigned long int size = (unsigned long int) 1 << (sizeof(addr_t)*8)>
            class AddressSpace :
            public Target<data_t, addr_t>,
            public Initiator<data_t, addr_t, nb_tgt>
            {

                /////////////
                // METHODS //
                /////////////

                public :

                ////////////////////////////////
                // Implement Target interface //
                ////////////////////////////////////////////////////////////////
                /**
                * \brief   See Target
                */
                virtual data_t* getPointer(const addr_t& address);

                /**
                * \brief   See Target
                */
                virtual const data_t read(const addr_t& address) const;

                /**
                * \brief   See Target
                */
                virtual bool write(const addr_t& address,
                                   const data_t& data);

                /**
                * \brief   See Target
                */
                virtual bool write(const addr_t& address,
                                   const data_t* data,
                                   const unsigned int data_size);
                ////////////////////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////
                /**
                 * \brief   Constructor
                 */
                AddressSpace();
                ////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////
                /**
                 * \brief   Destructor
                 */
                virtual ~AddressSpace();
                ////////////////////////////////////////////////////////////////


                ////////////////////////////////////////////////////////////////
                /**
                 * \brief   Map a target
                 *
                 * \param   target  the target to be mapped
                 * \param   offset  the offset (in bytes) where the target will
                 * be mapped (has to be alligned according to data_t size)
                 *
                 * This function maps the given Target at the given offset.
                 * \warning Note that an attempt to map a Target at an
                 * unalligned offset, or overlapping a previously mapped space,
                 * will cause the application to exit.
                 *
                 */
                virtual void map(Target<data_t, addr_t>& target,
                                 const addr_t& offset);
                ////////////////////////////////////////////////////////////////

                private :
                ////////////////////////////////////////////////////////////////
                /**
                 * \brief   sort a sub-array
                 *
                 * \param   left    the left index in the sub-array
                 * \param   right   the right index in the sub-array
                 * \param   p_index the pivot index in the sub-array
                 *
                 * \return  the new pivot index when sub-array is sorted
                 *
                 * This function realizes the "partition" operation of the quick
                 * sort algorithm
                 *
                 * \warning This function is for internal use only (used by map)
                 *
                 */
                const unsigned long int mapping_quick_sort_partition(
                                        const unsigned long int left,
                                        const unsigned long int right,
                                        const unsigned long int p_index);
                /**
                 * \brief   sort an array using the quick sort algorithm
                 *
                 * \param   left    the left index in the array
                 * \param   right   the right index in the array
                 *
                 * This function implements the quick sort algorithm
                 *
                 * \warning This function is for internal use only (used by map)
                 *
                 */
                void mapping_quick_sort(const unsigned long int left,
                                        const unsigned long int right);
                ////////////////////////////////////////////////////////////////

                ////////////////
                // ATTRIBUTES //
                ////////////////
                
                protected :
                
                // internal use only
                struct mappable_t
                {
                    addr_t start_address;             // in bytes
                    addr_t stop_address;              // in bytes
                    bool valid;

                    mappable_t(addr_t start = 0, addr_t stop = 0) :
                    start_address(start),
                    stop_address(stop),
                    valid(false)
                    {
                    };
                };

                // internal use only
                mappable_t mappable[nb_tgt];
            };
        }
    }
}

#endif
