#ifndef G_B_EMU_COMPONENTS_ADDRESSSPACE_HXX
#define G_B_EMU_COMPONENTS_ADDRESSSPACE_HXX

#include "addressspace.hpp"

namespace g_b
{
    namespace emu
    {
        namespace components
        {
            #define tmpl(...)\
            template<typename data_t,\
                     typename addr_t,\
                     const unsigned long int nb_tgt,\
                     const unsigned long int size>\
            __VA_ARGS__ AddressSpace<data_t, addr_t, nb_tgt, size>

            ////////////////////////////////////////////////////////////////////
            tmpl(data_t*)::getPointer(const addr_t& address)
            {
                assert(address%sizeof(data_t) == 0 &&
                "error : unaligned getPointer attempt");
 
                for(unsigned long int i = 0; i < this->getNbTarget(); ++i)
                {
                    if(address >= mappable[i].start_address &&
                       address <= mappable[i].stop_address)
                        return this->target[i]->getPointer(address - mappable[i].start_address);
                }

                assert(false &&
                "error : trying to get a pointer to an unmapped address");
            }
            ////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////
            tmpl(const data_t)::read(const addr_t& address) const
            {
                assert(address%sizeof(data_t) == 0 &&
                "error : unaligned read attempt");
 
                for(unsigned long int i = 0; i < this->getNbTarget(); ++i)
                {
                    if(address >= mappable[i].start_address &&
                       address <= mappable[i].stop_address)
                        return this->target[i]->read(address - mappable[i].start_address);
                }

                assert(false && "error : trying to read an unmapped address");
            }
            ////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////
            tmpl(bool)::write(const addr_t& address,
                              const data_t& data)
            {
                assert(address%sizeof(data_t) == 0 &&
                "error : unaligned write attempt");
 
                for(unsigned long int i = 0; i < this->getNbTarget(); ++i)
                {
                    if(address >= mappable[i].start_address &&
                       address <= mappable[i].stop_address)
                        return this->target[i]->write(address - mappable[i].start_address,
                                                data);
                }

                assert(false && "error : trying to write in an unmapped address");
            }
            ////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////
            tmpl(bool)::write(const addr_t& address,
                              const data_t* pdata,
                              const unsigned int data_size)
            {
                assert(address%sizeof(data_t) == 0 &&
                "error : unaligned write attempt");
 
                for(unsigned long int i = 0; i < this->getNbTarget(); ++i)
                {
                    if(address >= mappable[i].start_address &&
                       address <= mappable[i].stop_address)
                        return this->target[i]->write(address - mappable[i].start_address,
                                                pdata, data_size);
                }

                assert(false && "error : trying to write in an unmapped address");
            }
            ////////////////////////////////////////////////////////////////////
           
            ////////////////////////////////////////////////////////////////////
            tmpl()::AddressSpace() :
            Target<data_t, addr_t>(size)
            {
            }
            ////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////
            tmpl()::~AddressSpace()
            {
            }
            ////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////
            tmpl(void)::map(Target<data_t, addr_t>& tgt,
                            const addr_t& offset)
            {
                assert(offset%sizeof(data_t) == 0 &&
                "error : unaligned mapping attempt");
                assert(offset >= 0 &&
                "error : trying to map with a negative offset");
                assert(offset < this->size &&
                "error : trying to map with an offset greater than the address space's size");

                unsigned long int i = 0;

                // find first invalid "initiator port"
                while(mappable[i].valid)
                {
                    assert(i < this->getNbTarget()-1 &&
                    "error : AddressSpace's Initiator is too small");
                    ++i;
                }

                // then blindly insert
                mappable[i].valid = true;
                mappable[i].start_address = offset;
                mappable[i].stop_address = offset+tgt.getSize()-1;
                this->target[i] = &tgt;

                // then sort and check for overlap errors
                mapping_quick_sort(0,i);
            }
            ////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////
            tmpl(const unsigned long int)::mapping_quick_sort_partition(
                                            const unsigned long int left,
                                            const unsigned long int right,
                                            const unsigned long int p_index)
            {
                mappable_t              pivot;
                mappable_t              tmp_mappable;
                Target<data_t,addr_t>*  tmp_tgt;
                unsigned long int       store_index;

                // get pivot
                pivot = mappable[p_index];
                // swap pivot to the end of the array
                tmp_tgt                 = this->target[p_index];
                mappable[p_index]       = mappable[right];
                this->target[p_index]   = this->target[right];
                mappable[right]         = pivot;
                this->target[right]     = tmp_tgt;
                // init store index
                store_index = left;
                // let work the magic ...
                for(unsigned long int i = left; i < right; ++i)
                {
                    if(mappable[i].start_address < pivot.start_address)
                    {
                        assert(mappable[i].stop_address < pivot.start_address &&
                        "error : mapping overlap in AddressSpace");
                        // swap current element with the last stored index
                        tmp_mappable                = mappable[i];
                        tmp_tgt                     = this->target[i];
                        mappable[i]                 = mappable[store_index];
                        this->target[i]             = this->target[store_index];
                        mappable[store_index]       = tmp_mappable;
                        this->target[store_index]   = tmp_tgt;
                        // increment store index
                        ++store_index;
                    }
                    else
                        assert(mappable[i].start_address > pivot.stop_address &&
                        "error : mapping overlap in AddressSpace");
                }
                // ... and swap pivot back to its new place 
                tmp_mappable                = mappable[store_index];
                tmp_tgt                     = this->target[store_index];
                mappable[store_index]       = mappable[right];
                this->target[store_index]   = this->target[right];
                mappable[right]             = tmp_mappable;
                this->target[right]         = tmp_tgt;
                // return the new pivot index
                return store_index;
            }
            ////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////
            tmpl(void)::mapping_quick_sort(const unsigned long int left,
                                           const unsigned long int right)
            {
                // the array has at least 2 elements
                if(left < right)
                {
                    // call to partition with arbitrary pivot
                    unsigned long int new_pivot_index =
                    mapping_quick_sort_partition(left,right,(left+right)/2);
                    // recursive call on left array
                    mapping_quick_sort(left, new_pivot_index);
                    // recursive call on right array
                    mapping_quick_sort(new_pivot_index+1, right);
                }
            }
            ////////////////////////////////////////////////////////////////////

            #undef tmpl
        }
    }
}

#endif
