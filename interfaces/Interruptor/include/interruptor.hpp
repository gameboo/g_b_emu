#ifndef G_B_EMU_INTERFACES_INTERRUPTOR_HPP
#define G_B_EMU_INTERFACES_INTERRUPTOR_HPP

/**
 * \file    interruptor.hpp
 * \brief   Interruptor class
 * \author  Alexandre.JOANNOU
 * \version 0.1
 * \date    06/19/2012
 */

#include "Interruptible"
#include <cassert>

using namespace std;

namespace g_b
{
    namespace emu
    {
        namespace interfaces
        {
            /**
             * \class   Interruptor interruptor.hpp
             * \brief   Class representing an Interruptor
             *
             * An Initiator can interrupt an Interruptible component
             * \warning{
             * A component can be Interruptor \b AND Interruptible
             * }
             *
             */
            template<const unsigned int nb_interruptible = 1>
            class Interruptor
            {

                /////////////
                // METHODS //
                /////////////

                public :
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Constructor
                 */
                Interruptor();
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Destructor
                 */
                virtual ~Interruptor();
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   adds an Interruptible to the Interruptible array
                 *
                 * This methods adds the given Interruptible to the
                 * Interruptible array using the given index
                 *
                 */
                void addInterruptible(const unsigned long int index, Interruptible& i);
                ////////////////////////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   gets the number of Interruptible objects that can be
                 * connected
                 *
                 */
                const unsigned long int getNbInterruptible() const;
                ////////////////////////////////////////////////////////////////////
                
                ////////////////
                // ATTRIBUTES //
                ////////////////

                protected :

                Interruptible* interruptible[nb_interruptible];    /**< Interruptible Map */
            };
        }
    }
}

#endif
