#ifndef G_B_EMU_INTERFACES_INTERRUPTOR_HXX
#define G_B_EMU_INTERFACES_INTERRUPTOR_HXX

#include "interruptor.hpp"

namespace g_b
{
    namespace emu
    {
        namespace interfaces
        {
            #define tmpl(...)\
            template<const unsigned long int nb_interruptible>\
            __VA_ARGS__ Interruptor<nb_interruptible>

            ////////////////////////////////////////////////////////////////////////
            tmpl()::Interruptor()
            {
            }
            ////////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////////
            tmpl()::~Interruptor()
            {
            }
            ////////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////////
            tmpl(void)::addInterruptible(const unsigned long int index,
                                               Interruptible& i)
            {
                assert(index < getNbInterruptible()
                && "interruptor is too small");
                interruptible[index] = &i;
            }
            ////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////
            tmpl(inline const unsigned long int)::getNbInterruptible() const
            {
                return nb_interruptible;
            }
            ////////////////////////////////////////////////////////////////////////

            #undef tmpl
        }
    }
}

#endif
