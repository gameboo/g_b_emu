#ifndef G_B_EMU_INTERFACES_INITIATOR_HXX
#define G_B_EMU_INTERFACES_INITIATOR_HXX

#include "initiator.hpp"

namespace g_b
{
    namespace emu
    {
        namespace interfaces
        {
            #define tmpl(...)\
            template<typename data_t, typename addr_t, const unsigned int nb_tgt>\
            __VA_ARGS__ Initiator<data_t, addr_t, nb_tgt>

            ////////////////////////////////////////////////////////////////////////
            tmpl()::Initiator()
            {
            }
            ////////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////////
            tmpl()::~Initiator()
            {
            }
            ////////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////////
            tmpl(inline void)::setTarget(const unsigned int index, Target<data_t, addr_t>& tgt)
            {
                //TODO better way to control this ?
                assert(index < getNbTarget() && "initiator is too small");
                target[index] = &tgt;
            }
            ////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////
            tmpl(inline const unsigned long int)::getNbTarget() const
            {
                return nb_tgt;
            }
            ////////////////////////////////////////////////////////////////////////
     
            #undef tmpl
        }
    }
}

#endif
