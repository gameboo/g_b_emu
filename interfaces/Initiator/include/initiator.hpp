#ifndef G_B_EMU_INTERFACES_INITIATOR_HPP
#define G_B_EMU_INTERFACES_INITIATOR_HPP

/**
 * \file    initiator.hpp
 * \brief   Initiator class
 * \author  Alexandre.JOANNOU
 * \version 0.1
 * \date    06/19/2012
 */

#include "Target"
#include <cassert>

using namespace std;

namespace g_b
{
    namespace emu
    {
        namespace interfaces
        {
            /**
             * \class   Initiator initiator.hpp
             * \brief   Class representing an Initiator
             *
             * An Initiator can, through an AddressSpace, read from and/or write in
             * a Target
             * \warning{
             * A component can be an Initiator \b AND a Target
             * }
             *
             */
            template<typename data_t, typename addr_t, const unsigned int nb_tgt = 1>
            class Initiator
            {

                /////////////
                // METHODS //
                /////////////

                public :
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Constructor
                 */
                Initiator();
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Destructor
                 */
                virtual ~Initiator();
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   sets the Target at the given index
                 *
                 */
                virtual void setTarget(const unsigned int index,
                                       Target<data_t, addr_t>& tgt);
                ////////////////////////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Gets the number of Target in the Initiator
                 */
                const unsigned long int getNbTarget() const;
                ////////////////////////////////////////////////////////////////////
                
                ////////////////
                // ATTRIBUTES //
                ////////////////

                protected :

                Target<data_t,addr_t>* target[nb_tgt]; /**< Target array */
            };
        }
    }
}

#endif
