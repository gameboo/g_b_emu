#ifndef G_B_EMU_INTERFACES_ACTIVE_HXX
#define G_B_EMU_INTERFACES_ACTIVE_HXX

#include "active.hpp"

namespace g_b
{
    namespace emu
    {
        namespace interfaces
        {
            ////////////////////////////////////////////////////////////////////////
            Active::Active(const long double frequency = 1.0e6) :
            running(false)
            {
                //TODO check for 0 freq
                long double p = std::abs(1/frequency);
                period.tv_sec   = floor(p);
                period.tv_nsec  = (p - period.tv_sec) * 1e9;
            }
            ////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////
            Active::~Active()
            {
            }
            ////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////
            void Active::setFrequency(const long double frequency = 1.0e6)
            {
                //TODO check for 0 freq
                long double p = std::abs(1/frequency);
                period.tv_sec   = floor(p);
                period.tv_nsec  = (p - period.tv_sec) * 1e9;
            }
            ////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////
            void Active::createThread()
            {
                assert( !running && "thread already running");
                running = true;
                pthread_create(&thread, 0, Active::c_wrapper, this);
            }
            ////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////
            void Active::joinThread()
            {
                assert(running && "thread is not running");
                running = false;
                pthread_join(thread, 0);
            }
            ////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////
            void Active::cancelThread()
            {
                assert(running && "thread is not running");
                running = false;
                pthread_cancel(thread);
            }
            ////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////
            void* Active::c_wrapper(void* this_object)
            {
                reinterpret_cast<Active*>(this_object)->loop();
            }
            ////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////
            void Active::loop()
            {
                while(1)
                {
                    exec();
                    nanosleep(&period,NULL); //TODO use second argument
                }
            }
            ////////////////////////////////////////////////////////////////////////
        }
    }
}

#endif
