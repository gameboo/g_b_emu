#ifndef G_B_EMU_INTERFACES_ACTIVE_HPP
#define G_B_EMU_INTERFACES_ACTIVE_HPP

/**
 * \file    active.hpp
 * \brief   Active abstract class (need pthread)
 * \author  Alexandre.JOANNOU
 * \version 0.1
 * \date    06/19/2012
 */

#include <pthread.h>
#include <ctime>
#include <cmath>
#include <cassert>

/**
 * \namespace   g_b
 * \brief       gameboo namespace ;)
 */
namespace g_b
{

    /**
     * \namespace   emu
     * \brief       A namespace for emulation utils
     */
    namespace emu
    {
        /**
         * \namespace   interfaces
         * \brief       A namespace for emulation interfaces
         */
        namespace interfaces
        {
            /**
             * \class   Active active.hpp
             * \brief   Abstract class for "active"
             *  
             * This Abstract class provides basic support for an "active" component,
             * such as a threaded execution with parameterizable frequency.
             * This class is specialized by the Initiator class. You might also want
             * to create an "Active-Target" Target component.
             * \warning{
             * For an "Initiator-Target" component, remember that an Initiator
             * already inherit from Active
             * }
             *  
             */
            class Active
            {

                /////////////
                // METHODS //
                /////////////

                public :
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Constructor that initializes the frequency of the thread
                 *
                 * \param   frequency : the frequency of the thread
                 *
                 */
                Active(const long double frequency);
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Destructor
                 */
                virtual ~Active();
                ////////////////////////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Sets the frequency of the thread
                 *
                 * \param   frequency : the frequency of the thread
                 * 
                 */
                void setFrequency(const long double frequency);
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Launch the thread
                 *
                 * This method creates the thread and switches the #running
                 * attribute to true
                 *
                 */
                void createThread();
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Waits for the thread to end
                 *
                 * This method simply waits for the thread to end and returns
                 *
                 */
                void joinThread();
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Cancels the thread
                 *
                 * This method stops the thread and switches the #running attribute
                 * to false
                 *
                 */
                void cancelThread();
                ////////////////////////////////////////////////////////////////////
                
                protected :
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Function to be triggered when the thread wakes up
                 *
                 * This method has to be overrided to implement the desired
                 * component behaviour
                 *
                 */
                virtual void exec() = 0;
                ////////////////////////////////////////////////////////////////////
                
                ////////////////
                // ATTRIBUTES //
                ////////////////
                
                protected :
                
                pthread_t           thread;         /**< thread handler */
                volatile bool       running;        /**< thread state */
                struct timespec     period;         /**< thread sleeping period */
                
                private :
                
                /** \cond */    
                // C wrapper thread function for pthread
                static void* c_wrapper(void* this_object);
                // infinite exec loop wrapper
                void loop();
                /** \endcond */    
            };
        }
    }
}

#endif
