#ifndef G_B_EMU_INTERFACES_INTERRUPTIBLE_HXX
#define G_B_EMU_INTERFACES_INTERRUPTIBLE_HXX

#include "interruptible.hpp"

namespace g_b
{
    namespace emu
    {
        namespace interfaces
        {
            #define tmpl(...)\
            template<const unsigned int nb_interrupt> \
            __VA_ARGS__ Interruptible<nb_interrupt>
            ////////////////////////////////////////////////////////////////////////
            tmpl()::Interruptible()
            {
            }
            ////////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////////
            tmpl()::~Interruptible()
            {
            }
            ////////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////////
            tmpl(void)::interrupt(const unsigned long int index)
            {
                assert(index < nb_interrupt && "interrupt does not exist");
                interruption[index] = true;
            }
            ////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////
            tmpl(void)::resetInterrupt(const unsigned long int index)
            {
                assert(index < nb_interrupt && "interrupt does not exist");
                interruption[index] = false;
            }
            ////////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////////
            tmpl(const unsigned long int)::getInterruptNb() const
            {
                return nb_interrupt;
            }
            ////////////////////////////////////////////////////////////////////////

            #undef tmpl

        }
    }
}

#endif
