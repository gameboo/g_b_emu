#ifndef G_B_EMU_INTERFACES_INTERRUPTIBLE_HPP
#define G_B_EMU_INTERFACES_INTERRUPTIBLE_HPP

/**
 * \file    interruptible.hpp
 * \brief   Interruptible class
 * \author  Alexandre.JOANNOU
 * \version 0.1
 * \date    06/19/2012
 */

#include <cassert>

namespace g_b
{
    namespace emu
    {
        namespace interfaces
        {
            /**
             * \class   Interruptible interruptible.hpp
             * \brief   Class representing an Interruptible component
             *
             * An Interruptible can be interrupted
             * \warning{
             * A component can be Interruptible \b AND Interruptor
             * }
             *
             */
            template<const unsigned int nb_interrupt = 1>
            class Interruptible
            {

                /////////////
                // METHODS //
                /////////////

                public :
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Constructor
                 */
                Interruptible();
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Destructor
                 */
                virtual ~Interruptible();
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Interrupt the component
                 *
                 * This methods position the concerned interruption bit in the
                 * interruption array to true
                 *
                 */
                virtual void interrupt(const unsigned long int index = 0);
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   reset the interrupt
                 *
                 * This methods position the concerned interruption bit in the
                 * interruption array to false
                 *
                 */
                virtual void resetInterrupt(const unsigned long int index = 0);
                ////////////////////////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   returns the number of supported interrupt(s)
                 *
                 */
                const long unsigned int getInterruptNb() const;
                ////////////////////////////////////////////////////////////////////
                
                ////////////////
                // ATTRIBUTES //
                ////////////////

                protected :

                bool interruption[nb_interrupt];    /**< The interruption array*/
            };
        }
    }
}

#endif
