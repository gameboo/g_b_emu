#ifndef G_B_EMU_INTERFACES_TARGET_HPP
#define G_B_EMU_INTERFACES_TARGET_HPP

/**
 * \file    Target.hpp
 * \brief   Target class
 * \author  Alexandre.JOANNOU
 * \version 0.1
 * \date    06/19/2012
 */

#include <cassert>

namespace g_b
{
    namespace emu
    {
        namespace interfaces
        {
            /**
             * \class   Target target.hpp
             * \brief   Class representing a Target
             *
             * A Target can be read and/or written by an Initiator
             * \warning{
             * \li A component can be an Active Target
             * \li A component can be an Initiator and a Target
             * }
             *
             */
            template<typename data_t, typename addr_t>
            class Target
            {
                /////////////
                // METHODS //
                /////////////

                public :
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Constructor
                 *
                 * \param   size : the desired size IN BYTES
                 *
                 */
                Target(const unsigned long int size);
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Destructor
                 */
                virtual ~Target();
                ////////////////////////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Gets a "C style" pointer to the desired address
                 *
                 * \param   address: the address to be read (byte address)
                 *
                 * \return  A pointer to the given address
                 *
                 * This fuction returns a pointer to the given address
                 *
                 */
                virtual data_t* getPointer(const addr_t& address) = 0;
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Reads this component
                 *
                 * \param   address: the address to be read (byte address)
                 *
                 * \return  the data stored at the given address
                 *
                 * This function returns the data read at the given address
                 *
                 */
                virtual const data_t read(const addr_t& address) const = 0;
                ////////////////////////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Writes in this component
                 *
                 * \param   address:    the address where to write (byte address)
                 * \param   data:       the data to be written
                 *
                 * \return  true if the write occured correctly
                 *
                 * This method writes the given data at the given address
                 *
                 */
                virtual bool write(const addr_t& address, const data_t& data) = 0;
                ////////////////////////////////////////////////////////////////////
                
                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Writes in this component
                 *
                 * \param   address:    the address where to write (byte address)
                 * \param   data_ptr:   a pointer to the data to write
                 * \param   size:       the size of the data (in bytes)
                 *
                 * \return  true if the write occured correctly
                 *
                 * This writes the given data at the given address 
                 *
                 */
                virtual bool write(const addr_t& address,
                                   const data_t* data_ptr,
                                   const unsigned int size) = 0;
                ////////////////////////////////////////////////////////////////////

                ////////////////////////////////////////////////////////////////////
                /**
                 * \brief   Gets the size of the target (size in bytes)
                 */
                const unsigned long int getSize() const;
                ////////////////////////////////////////////////////////////////////

                ////////////////
                // ATTRIBUTES //
                ////////////////

                protected :
                const unsigned long int size;
            };
        }
    }
}

#endif
