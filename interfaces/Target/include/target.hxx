#ifndef G_B_EMU_INTERFACES_TARGET_HXX
#define G_B_EMU_INTERFACES_TARGET_HXX

#include "target.hpp"

namespace g_b
{
    namespace emu
    {
        namespace interfaces
        {
            #define tmpl(...)\
            template<typename data_t, typename addr_t>\
            __VA_ARGS__ Target<data_t, addr_t>

            ////////////////////////////////////////////////////////////////////////
            tmpl()::Target(const unsigned long int s) :
            size(s)
            {
                assert(s%sizeof(data_t) == 0 &&
                "error : a Target size must be a multiple of the data type size");
            }
            ////////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////////
            tmpl()::~Target()
            {
            }
            ////////////////////////////////////////////////////////////////////////
            
            ////////////////////////////////////////////////////////////////////////
            tmpl(const unsigned long int)::getSize() const
            {
                return size;
            }
            ////////////////////////////////////////////////////////////////////////
     
            #undef tmpl
        }
    }
}

#endif
