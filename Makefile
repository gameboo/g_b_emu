export DEBUG	=	yes

INCLUDE_DIR		=	include
LIB_DIR			=	lib
INTERFACES_DIR	=	interfaces
COMPONENTS_DIR	=	components
EXAMPLES_DIR	=	examples

export CC		=	gcc
export CXX		=	g++

ifeq ($(DEBUG),yes)
export CFLAGS	=	-Werror -Wfatal-errors -g
export CXXFLAGS	=	-Werror -Wfatal-errors -g
else
export CFLAGS	=
export CXXFLAGS	=
endif

all : interfaces_ components_ #examples_ #doc_

interfaces_ :
	cd $(INTERFACES_DIR) && $(MAKE)

components_ : interfaces_
	cd $(COMPONENTS_DIR) && $(MAKE)
	#todo
	#mkdir -p $(LIB_DIR)
	#ar -c ... with an editable component list  

examples_ : components_
	cd $(EXAMPLES_DIR) && $(MAKE)

doc_ :
	cd $(COMPONENTS_DIR) && $(MAKE) doc_
	doxygen

.PHONY : clean interfaces_clean components_clean examples_clean doc_clean

clean : interfaces_clean components_clean examples_clean doc_clean
	rm -rf $(LIB_DIR)
	rm -rf $(INCLUDE_DIR)

interfaces_clean :
	cd $(INTERFACES_DIR) && $(MAKE) clean

components_clean :
	cd $(COMPONENTS_DIR) && $(MAKE) clean

examples_clean :
	cd $(EXAMPLES_DIR) && $(MAKE) clean

doc_clean :
	cd $(COMPONENTS_DIR) && $(MAKE) doc_clean
	rm -rf doc

dist :
	#todo

tar :
	#todo

check :
	#todo

distcheck :
	#todo

install :
	#todo

uninstall :
	#todo

distclean :
	#todo
